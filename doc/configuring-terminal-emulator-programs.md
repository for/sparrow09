Configuring terminal emulator programs
======================================

So far, we've got hints for these terminal emulator programs:

- [tio](https://tio.github.io/)
- [minicom](https://salsa.debian.org/minicom-team/minicom/)

Configuring tio
===============

It doesn't get any simpler than this: just call tio with these command
line parameters.

```
tio -b 38400 -o1 -m INLCRNL /dev/ttyUSB1
```


Configuring minicom
===================

This document gives you some hints on how to configure minicom for a
flawless session with the sparrow single-board computer. Minicom is a
text-based modem control and terminal emulator program for Unix-like
operating systems including Linux and macOS and modeled after the
once popular MS-DOS program Telix.


Linefeed
--------

If you get a screen like this:

```
     .-.

    /'v'\       SPARROW

   (/   \)      CPU 6309 @ 3.7 Mhz, 32 KB ROM, 512 KB RAM

  ='="="===<    https://codeberg.org/for/sparrow09

     |_|



Welcome to BUGGY version 1.0
```

start minicom and press Ctrl-A, then Z to enter the minicom main menu, then
press A for "add linefeed" and you should get a boot screen like this:

```
     .-.
    /'v'\       SPARROW
   (/   \)      CPU 6309 @ 3.7 Mhz, 32 KB ROM, 512 KB RAM
  ='="="===<    https://codeberg.org/for/sparrow09
     |_|

Welcome to BUGGY version 1.0
```


Pasting S-records
-----------------

When pasting S-records from the clipboard, minicom may send the characters
too fast for the sparrow on the other end, ending up with broken records
like so:

```
S1134000C6419D03C6429D03C6439D033F00000075
S114010000000000000000000000000000000009C
Error in S record
S90
Error in S record
```

You might want to add a delay of 1 ms between each character to fix this
issue. Press Ctrl-A, then Z to enter the minicom main menu, then T, then F
and enter '1' ms:

```
              ┌───────────────[Terminal settings]────────────────┐
              │                                                  │
              │ A -      Terminal emulation : VT102              │
              │ B -     Backspace key sends : DEL                │
              │ C -          Status line is : enabled            │
              │ D -   Newline tx delay (ms) : 0                  │
              │ E -          ENQ answerback : Minicom2.8         │
              │ F - Character tx delay (ms) : 1                  │
              │    Change which setting?                         │
              │                                                  │
              └──────────────────────────────────────────────────┘
```

The additional delay should give sparrow enough time to process the input
and to accept the S-records without errors:

```
S1134000C6419D03C6429D03C6439D033F00000075
S1134010000000000000000000000000000000009C
S9030000FC
```

Config file
-----------

You may save some settings in the config file /etc/minirc.sparrow and start
minicom with `minicom sparrow09` to start with your personal settings.
However I haven't yet found out how to configure the character delay in the
config file, so you'll still have to do this manually.

```
 minicom terminal program configuration file
# Linux users: copy this file to /etc/minirc.sparrow09
# Start minicom with:
#
#       minicom sparrow09
#
pu minit
pu mreset
pu mhangup
pu port         /dev/ttyUSB1
pu baudrate     38400
pu bits         8
pu parity       N
pu stopbits     1
pu rtscts       No
pu xonxoff      No
pu localecho    No
pu linewrap     Yes
pu addcarreturn Yes
```

