GAL equations
=============

The JEDEC file was assembled with GALasm, freely available from
https://github.com/nils-eilers/GALasm or https://github.com/daveho/GALasm


- Addresses are only valid when clock line E is high.
- RW is high for read access, low on writes.

```
/RD =  RW * E
/WR = /RW * E

/FDC  = E * /CS * /A1 * /A2 * /A3 * /A4
/LDOR = E * /CS *  A1 * /A2 * /A3 * /A4
/LDCR = E * /CS * /A1 *  A2 * /A3 * /A4

/DACK = LDOR + /RW

RESH = /RESL

D0.T = DSKCHG
D0.E = E * RW * /CS
```

                                GAL16V8

                          -------\___/-------
                        E |  1           20 | VCC
                          |                 |
                       RW |  2           19 | RD
                          |                 |
                     RESL |  3           18 | WR
                          |                 |
                       CS |  4           17 | FDC
                          |                 |
                       A1 |  5           16 | LDOR
                          |                 |
                       A2 |  6           15 | LDCR
                          |                 |
                       A3 |  7           14 | RESH
                          |                 |
                       A4 |  8           13 | DACK
                          |                 |
                       NC |  9           12 | D0
                          |                 |
                      GND | 10           11 | DSKCHG
                          -------------------
