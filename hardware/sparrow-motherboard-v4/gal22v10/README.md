GAL equations
=============

The JEDEC file was assembled with GALasm, freely available from
https://github.com/nils-eilers/GALasm or https://github.com/daveho/GALasm


- Addresses are only valid when clock line E is high.
- RW is high for read access, low on writes.
- FEFFXX is low for addresses in the $FE00-$FFFF range. Please note that
  this not the actual I/O range which would be $FE00-$FEFF only.
- ROMON is an output port of the DUART and defaults to high on power-on or
  resets. If ROMON is high, the ROM will be available for read access from
  $8000-$FDFF and $FF00-$FFFF. RAM is always available from $0000-$7FFF and
  will be available too from $8000-$FDFF when ROMON is low. ROM is always
  readable from $FF00-$FFFF regardless the state of ROMON in order to
  always provide valid interrupt vectors.  Write access always enables the
  underlying RAM even if ROMON is high so that you can easily copy ROM
  contents to the underlying RAM by simply reading bytes from ROM and writing
  them to the RAM at the same address.
- The DUART address range ($FEF0-$FEFF) is mirrored to $FEE0-$FEEF, so that
  accessing the DUART by $FEE0-$FEEF will generate a high pulse on SDCK,
  whereas accesses from $FEF0 to $FEFF don´t.  SPI access works by first
  outputting the data bit SDDI without generating a clock pulse on SDCK, then
  reading from the card by reading SDDO from the DUART input port at the
  other address area, causing a high pulse on SDCK.
- IOEN is high when addresses are valid (E=1) and addresses are in the
  range from $FE00 to $FEFF.


                                GAL22V10

                          -------\___/-------
                        E |  1           24 | VCC
                          |                 |
                   FEFFXX |  2         8 23 | OE
                          |                 |
                    DUART |  3        10 22 | WE
                          |                 |
                       RW |  4        12 21 | IOEN
                          |                 |
                      A15 |  5        14 20 | RA18
                          |                 |
                       A8 |  6        16 19 | ROMCS
                          |                 |
                       A4 |  7        16 18 | RAMCS
                          |                 |
                    ROMON |  8        14 17 | RA17
                          |                 |
                       B0 |  9        12 16 | RA16
                          |                 |
                       B1 | 10        10 15 | SDCK
                          |                 |
                       B2 | 11         8 14 | A14
                          |                 |
                      GND | 12           13 | A13
                          -------------------

Note: the inner numbers designate the number of available product terms for
this output pin

