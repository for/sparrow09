Sparrow Motherboard
===================

Specifications:

- CPU Hitachi 6309 @ 3.6864 MHz
- 32 KB ROM
- Up to 512 KB RAM
- USB Serial Port
- RS-232 Serial Port
- SD-Card
- 4 Expansion Slots (expandable to 7)


![Photo of Sparrow Motherboard](sparrow-v4-render.jpg)
