Sparrow PS2 Keyboard- and Mouse-Interface
=========================================

This board also contains a battery buffered real-time-clock.

![Photo of Sparrow PS/2 Interface](Sparrow_PS2_Interface_1.jpg)
![Photo of Sparrow PS/2 Interface](Sparrow_PS2_Interface_2.jpg)
