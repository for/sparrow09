Sparrow Audio/Video Card
========================

The Audio/Video Card combines a Yamaha V9958 Video Display Processor
with an Yamaha OPL2 sound chip YM3812, both connected to a SCART socket.

Specifications V9958:

- Video RAM 128 KB
- Text modes: 80x24 and 32x24
- Resolution: 512 x 212 (4 or 16 colors out of 512) and 256 x 212 (16, 256, 12499 or 19268 colors)
- Sprites: 32, 16 colors, max. 8 per hoizontal line
- Hardware acceleration for copy, line, fill, etc.
- Interlacing to double vertical resolution
- Horizontal and vertical scroll registers

![Photo of Sparrow AV Card](Sparrow_AV_Card.jpg)
