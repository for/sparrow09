Your own shiny new expansion card!
==================================

This is a project skeleton which you can use as a starting point for your
own new project, your own new expansion card.

If you're on some sort of \*NIX, Linux or macOS (formerly known as OS X), please rename the project with the provided shell script:

        ./rename-project.sh <your-new-name>


It will rename all relevant files with the exception of the project folder
itself.

BTW: inside the kicad folder, there's a truetype font called *LET DER B
CARNAGE!!! (Regular).ttf*. This font has been used for some existing
expansion cards and if you like them, you may want to use it in your own projects too.

However, you really should rewrite this README:

Tell us about your new expansion, what's it good for?

![Photo of empty new expansion cards](skeleton.png)
