#!/bin/sh
mv ./kicad/oldname.kicad_pcb ./kicad/$1.kicad_pcb
mv ./kicad/oldname.kicad_pro ./kicad/$1.kicad_pro
mv ./kicad/oldname.kicad_prl ./kicad/$1.kicad_prl
mv ./kicad/expansion-gerbers ./kicad/$1-gerbers
mv ./kicad/$1-gerbers/oldname-job.gbrjob ./kicad/$1-gerbers/$1-job.gbrjob
mv ./kicad/$1-gerbers/oldname-NPTH.drl ./kicad/$1-gerbers/$1-NPTH.drl
mv ./kicad/$1-gerbers/oldname-B_Cu.gbr ./kicad/$1-gerbers/$1-B_Cu.gbr
mv ./kicad/$1-gerbers/oldname-F_Mask.gbr ./kicad/$1-gerbers/$1-F_Mask.gbr
mv ./kicad/$1-gerbers/oldname-B_Mask.gbr ./kicad/$1-gerbers/$1-B_Mask.gbr
mv ./kicad/$1-gerbers/oldname-F_Silkscreen.gbr ./kicad/$1-gerbers/$1-F_Silkscreen.gbr
mv ./kicad/$1-gerbers/oldname-Edge_Cuts.gbr ./kicad/$1-gerbers/$1-Edge_Cuts.gbr
mv ./kicad/$1-gerbers/oldname-B_Silkscreen.gbr ./kicad/$1-gerbers/$1-B_Silkscreen.gbr
mv ./kicad/$1-gerbers/oldname-F_Cu.gbr ./kicad/$1-gerbers/$1-F_Cu.gbr
mv ./kicad/$1-gerbers/oldname-PTH.drl ./kicad/$1-gerbers/$1-PTH.drl
mv ./kicad/oldname-tracks-config ./kicad/$1-tracks-config
mv ./kicad/oldname.kicad_sch ./kicad/$1.kicad_sch
