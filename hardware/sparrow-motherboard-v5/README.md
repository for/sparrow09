Sparrow Motherboard
===================

Specifications:

- CPU Hitachi 6309E @ 4 MHz
- 128 KB ROM
- 512 KB RAM
- USB Serial Port
- RS-232 Serial Port
- Two SD-Card Slots
- 6 Expansion Slots

