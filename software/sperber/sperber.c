#if defined(__linux__)
#define TERMINAL    "/dev/sparrow09"
#else
#define TERMINAL    "/dev/cu.usbserial-sp00004"
#endif

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <ctype.h>
#include <dirent.h>

int Debug;               // run with "-d" for debug
int fd;                  // terminal file descriptor
#define COLUMNS 85
char command[COLUMNS+1];
char filename[COLUMNS+1];
char report[COLUMNS+1];

// sparrow load file structure
// ---------------------------
// byte   0 : S
// byte   1 : bank
// word 2-3 : start address
// Word 4-5 : n = length of data section
// word 6-7 : 16 bit checksum of data
// data     : n bytes
// for odd file length, the last byte is ignored in the checksum

// MAX_FILE_SIZE = 8 bytes header + data
// data load area = $2001 -> $FDFF

#define MAX_FILE_SIZE 0xde08

unsigned char Image[MAX_FILE_SIZE];

#define MAX_DIR_ENTRIES 125
#define MAX_DIR_FN      17

char Dir[MAX_DIR_ENTRIES][MAX_DIR_FN+1];

// *********
// SkipSpace
// *********

char *SkipSpace(char *p)
{
   if (*p) while (isspace(*p)) ++p;
   return p;
}


// Compare two strings ignoring case of max. length n

// ***********
// StrNCaseCmp
// ***********

int StrNCaseCmp(const char *a, const char *b, unsigned int n)
{
   unsigned int i,l,m;

   l = strlen(a);
   m = strlen(b);

   for (i=0 ; i < n && i <= l && i <= m ; ++i)
   {
      if (toupper(a[i]) < toupper(b[i])) return -1;
      if (toupper(a[i]) > toupper(b[i])) return  1;
   }
   return 0;
}

// *********************
// set_interface_attribs
// *********************

int set_interface_attribs(int fd, int speed)
{
    struct termios tty;

    if (tcgetattr(fd, &tty) < 0)
    {
        printf("Error from tcgetattr: %s\n", strerror(errno));
        return -1;
    }

    cfsetospeed(&tty, (speed_t)speed);
    cfsetispeed(&tty, (speed_t)speed);

    cfmakeraw(&tty);

    /* fetch bytes as they become available */
    tty.c_cc[VMIN] = 1;
    tty.c_cc[VTIME] = 1;

    if (tcsetattr(fd, TCSANOW, &tty) != 0)
    {
        printf("Error from tcsetattr: %s\n", strerror(errno));
        return -1;
    }
    return 0;
}

// ***********
// Get_Command
// ***********

char Get_Command(int fd)
{
   int rdlen,len;
   unsigned char b;

   len = 0;
   do
   {
      rdlen = read(fd, &b, 1);
      if (rdlen == 1)
      {
         if ( b < ' ') break;
         if (b < 0x7f) command[len++] = b;
      }
      else if (rdlen < 0) printf("Error: %s\n",strerror(errno));
   }  while (len < COLUMNS);
   command[len] = 0;
   puts(command);
   return command[0];
}

const char *str_status = "File Server Sperber\n";
const char *str_syntax = "Server Syntax Error\n";

void SyntaxError(void)
{
   write(fd,str_syntax,strlen(str_syntax)+1);
}

void WaitForSparrow(char *msg)
{
   unsigned char begin = 0x81;
   unsigned char b;
   int rdlen;

   if (msg)
   {
       puts(msg);
       fflush(stdout);
   }
   do
   {
      rdlen = read(fd, &b, 1);
   }  while (rdlen == 0 || b != begin);
   write(fd,&begin,1); // start transmission
   if (msg) puts(msg);
}

void SendSparrowChar(unsigned char c)
{
   write(fd,&c,1);
}

void SendSparrowString(unsigned char *s)
{
   while (*s)
   SendSparrowChar(*s++);
}

int CheckSum(unsigned char* a, int n)
{
   int sum = 0;
   int i;
   for (i=8 ; i<n+7 ; i+=2)
      sum += (a[i] << 8) | a[i+1];
   return sum & 0xffff;
}

int CheckLength(int n)
{
   unsigned char *q,*lp,*maxlp;
   int ok,ll,maxll,lc,maxlc;

   q = Image;
   maxll = 0;
   maxlp = q;
   lc = 0;
   maxlc = 0;
   ok = 1;
   while (q < Image + n)
   {
      ll = 0;
      lp = q;
      ++lc;
      while (isspace(*q) && q < Image + n) {++ll;++q;} // leading space
      while (isdigit(*q) && q < Image + n) {++ll;++q;} // BASIC line number
      while (*q >= ' ' && q < Image + n)   {++ll;++q;} // BASIC program line
      if (ll > maxll) {maxll = ll; maxlp = lp; maxlc = lc;}
      while (*q < ' ' && q < Image + n) ++q;          // terminators
   }
   if (maxll > 84)
   {
      sprintf(report,"Line %d too long",maxlc);
      ok = 0;
   }
   if (Debug || ok == 0)
   {
      printf("Longest line %d has %d characters\n",maxlc,maxll);
      printf("[");
      for (ll = 0 ; ll < maxll ; ++ll)
         printf("%c",maxlp[ll]);
      printf("]\n");
   }
   return ok;
}

// ********
// LoadFile
// ********

void LoadFile(char *p)
{
   size_t mf,items;
   size_t l;
   FILE *lf;
   int i,n,rdlen;
   int ok;
   unsigned char nhi,nlo,binflag;
   unsigned char begin  = 0x81;
   char loim[8];

   strcpy(loim,"Load");

   items = MAX_FILE_SIZE;
   memset(Image,0,sizeof(Image));

   while (*p && *p != '"') ++p;

   if (*p == '"')
   {
      if (strlen(p) > 4 && p[3]=='0' && p[4]==':') p+=4;
      strcpy(filename,p+1);
      if (Debug) printf("filename = <%s>\n",filename);
      l = strlen(filename);
      if (l && filename[l-1] <  ' ') filename[--l] = 0; // string delimiter
      if (l && filename[l-1] == '"') filename[--l] = 0; // name   delimiter

      lf = fopen(filename,"rb");
      ok = 1;
      if (lf)
      {
         n = fread(Image,1,items,lf);
         if (ferror(lf))
         {
            sprintf(report,"[%s]\n",strerror(errno));
            ok = 0;
         }
         else sprintf(report,"Load <%s> %d bytes",filename,n);
         fclose(lf);

         binflag = 0;
         for (i=0 ; i < n ; ++i)
            if (Image[i] < 0x20 && Image[i] != 13 && Image[i] != 10) binflag = 1;

         // check import line lenghts, must be < 85 characters

         if (ok && binflag==0) ok = CheckLength(n);

         if (ok)
         {
            printf("%s <%s> %d bytes\n",loim,filename,n);
            fflush(stdout);
            sprintf(report,"%s <%s> %d bytes\n",loim,filename,n);
         }
         WaitForSparrow(NULL);
         nhi = n >>  8;
         nlo = n & 255;
         binflag |= '0';
         write(fd,&binflag,1);
         write(fd,&nhi,1);
         write(fd,&nlo,1);
         for (i=0 ; i < n ; ++i)
         {
            write(fd,Image+i,1);
         }
      }
      else
      {
         sprintf(report,"<%s>\n",strerror(errno));
      }
      fputs(report,stdout);
      WaitForSparrow(NULL);
      write(fd,report,strlen(report)+1);
//    fputs(report,stdout);
   }
}

void SaveFile(char *p)
{
   size_t mf,items;
   size_t l;
   FILE *lf;
   int i,n,rdlen;
   unsigned char begin  = 0x81;
   unsigned char b,nhi,nlo;
   char *fnp;


   items = MAX_FILE_SIZE;
   memset(Image,0,sizeof(Image));

   while (*p && *p != '"') ++p;
   if (*p == '"')
   {
      if (strlen(p) > 4 && p[3]=='0' && p[4]==':') p+=4;
      strcpy(filename,p+1);
      l = strlen(filename);
      if (l && filename[l-1] <  ' ') filename[--l] = 0; // string delimiter
      if (l && filename[l-1] == '"') filename[--l] = 0; // name   delimiter
      fnp = filename;

      WaitForSparrow(NULL);

      read(fd,&nhi,1);
      read(fd,&nlo,1);
      items = (nhi << 8) | nlo;

      for (i=0 ; i < items ; ++i)
      {
         read(fd,Image+i,1);
      }

      errno = 0;
      if (Debug) printf("fopen(%s,wb)",fnp);
      lf = fopen(fnp,"wb");

      if (errno)
      {
         sprintf(report,"[%s]\n",strerror(errno));
         errno = 0;
      }
      else if (lf)
      {
         n = fwrite(Image,1,items,lf);
         if (ferror(lf))
         {
             sprintf(report,"[%s]\n",strerror(errno));
         }
         else
         {
             sprintf(report,"Saved <%s> %d bytes",fnp,n);
         }
         fclose(lf);
      }
      fputs(report,stdout);
      WaitForSparrow(NULL);
      write(fd,report,strlen(report)+1);
      fputc('\n',stdout);
   }
}

int CmpDir( const void *arg1, const void *arg2 )
{
   return strcmp(arg1,arg2);
}

void Directory(char *p)
{
   int i,j,l,n;
   DIR *d;
   struct dirent *dir;
   memset(Dir,0,sizeof(Dir));
   n = 0;
   d = opendir(".");
   if (d)
   {
       while ((dir = readdir(d)) != NULL && n < MAX_DIR_ENTRIES)
       {
          if (dir->d_name[0] != '.')
          {
             if (dir->d_type == DT_DIR) Dir[n][0] = '0';
             else                       Dir[n][0] = '1';
             strncpy(Dir[n]+1,dir->d_name,MAX_DIR_FN-2);
             if (strlen(dir->d_name) > MAX_DIR_FN-1) Dir[n][MAX_DIR_FN-2] = '~';
             ++n;
          }
       }
       closedir(d);
   }

   qsort(Dir,n,MAX_DIR_FN+1,CmpDir);

   WaitForSparrow(NULL);
   for (i=0 ; i < n ; ++i)
   {
      printf("%-17s",Dir[i]+1);
      SendSparrowChar(0x4d-Dir[i][0]); // set bold ($1D) for dir
      SendSparrowString((unsigned char *)Dir[i]+1);
      if ((i % 5) == 4 || i == n-1)
      {
         printf("\n");
         SendSparrowChar('\n');
         SendSparrowChar('\0');
         WaitForSparrow(NULL);
      }
      else
      {
         l = strlen(Dir[i]);
         for (j=0 ; j < (MAX_DIR_FN-l) ; ++j) SendSparrowChar(' ');
      }
   }
   SendSparrowChar(0x82);
}

int DispatchCommand(void)
{
   int r=1;
   char *p = command;
   if (command[0] == '@') p = SkipSpace(p+1);
        if (!StrNCaseCmp(p,"LOAD"  ,4)) LoadFile(p);
   else if (!StrNCaseCmp(p,"RUN"   ,3)) LoadFile(p);
   else if (!StrNCaseCmp(p,"SAVE"  ,4)) SaveFile(p);
   else if (!StrNCaseCmp(p,"DIR",3)) Directory(p);
   else
   {
      printf("Syntax error: <%s>\n",command);
      r = 0;
   }
   return r;
}


int main(int argc, char *argv[])
{
    char *portname = TERMINAL;
    char com;
    int ok;

    if (argc > 1 && !strncmp(argv[1],"-d",2)) Debug = 1;
    if (Debug) printf("Debug output enabled\n");

    fd = open(portname, O_RDWR | O_NOCTTY | O_SYNC);
    if (fd < 0)
    {
        printf("Error opening %s: %s\n", portname, strerror(errno));
        return -1;
    }
    set_interface_attribs(fd, B115200);
    tcdrain(fd);

    do
    {
       com = Get_Command(fd);
       ok = DispatchCommand();
    }  while (com != 'Q' && ok);
}

