  100 ' Bubble universe as seen on https://www.pouet.net/prod.php?which=95417
  110 n%=200
  120 r=PI*2/235
  130 x=0:y=0:v=0:t=0
  140 s%=60
  160 CLS
  210 FOR i%=0 TO n%
  220 FOR j%=0 TO n%
  230 u=SIN(i%+v)+SIN(r*i%+x)
  240 v=COS(i%+v)+COS(r*i%+x)
  250 x=u+t
  260 a%=40+INT((2+u)*s%):b%=INT((2-v)*s%)
  270 c%=2
  280 IF i%>=100 THEN c%=c%+1
  290 IF j%>=100 THEN c%=c%+2
  300 DOT a%,b%,c%
  310 NEXT
  320 NEXT
  340 t=t+.025
  350 GOTO 210
