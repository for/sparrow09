   20 CLS
   30 x1%=511:y1%=211
   40 i1=-1.0:i2=1.0:r1=-2:r2=1.0
   50 s1=(r2-r1)/x1%:s2=(i2-i1)/y1%
   60 FOR y%=0 TO y1%
   70 i3=i1+s2*y%
   80 FOR x%=0 TO x1%
   90 r3=r1+s1*x%:z1=r3:z2=i3
  100 FOR n%=0 TO 30
  110 a=z1*z1:b=z2*z2
  120 IF a+b>4.0 GOTO 150
  130 z2=2*z1*z2+i3:z1=a-b+r3
  140 NEXT
  150 DOT x%,y%,n% AND 15
  160 NEXT
  170 NEXT
