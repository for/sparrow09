***************
module  Sperber
***************
        ldx     #LINEBUF
.loop   lda     ,x+
        beq     .err
        cmpa    #'@'
        beq     .loop
        cmpa    #' '
        beq     .loop
        ANDA    #%1101 1111     ; force upper case
        cmpa    #'L'
        lbeq    Sperber_Load
        cmpa    #'S'
        lbeq    Sperber_Save
        cmpa    #'D'
        beq     Sperber_Dir
.err    coma
        rts
endmod

******************
module  Mon_USB_In
******************
        ldb     #%00000001      ; RxRDYA
.wait   bitb    DUART_SRA
        beq     .wait
        lda     DUART_RHR
        rts
endmod

*******************
module  Mon_USB_Out
*******************
        pshs    b
        ldb     #%00000100      ; TxRDYA
.wait   bitb    DUART_SRA
        beq     .wait
        sta     DUART_THR
        puls    b,pc
endmod

***********************
module  Sperber_Send_FN
**********************
        pshs    cc,d,x
        orcc    #IRQFLAG|FIRQFLAG ; disable interrupts
.cloop  lda     ,x+
        beq     .laba
        bsr     Mon_USB_Out
        bra     .cloop

.laba   lda     #QUOTE
        bsr     Mon_USB_Out
        ldx     #FILENAME
.wloop  lda     ,x+
        bsr     Mon_USB_Out
        tsta
        bne     .wloop
        lda     #QUOTE
        bsr     Mon_USB_Out
        clra
        bsr     Mon_USB_Out
        puls    cc,d,x,pc

*******************
module  Sperber_Dir
*******************

* send command

        ldx     #.dir
        bsr     Sperber_Send_FN

* receive directory

.next   pshs    cc
        orcc    #IRQFLAG|FIRQFLAG ; disable interrupts

.waitok lda     #$81
        bsr     Mon_USB_Out
        bsr     Mon_USB_In
        beq     .waitok
        cmpa    #$81            ; start of transmission
        bne     .err

        ldx     #LINEBUF
.line   bsr     Mon_USB_In
        sta     ,x+
        beq     .eol
        cmpa    #$82            ; end of transmission
        beq     .ret
        cmpx    #LINEBUF+COLUMNS
        blo     .line

.eol    puls    cc
        ldx     #LINEBUF
        CALL PUTS
        bra     .next

.ret    puls    cc
        clra                    ; clear carry
        rts

.err    puls    cc
        coma                    ; set carry
        rts
.dir    byte    "DIR",0
endmod

********************
module  Mem_Checksum
*****************************
* Input : Y = start address *
*         F = bank          *
*         D = count         *
* Output: D = checksum      *
*****************************
        pshs    x,y,u
        lsrd
        tfr     d,x             ; count words
        subr    u,u             ; clear u
.loop   jsr     Far_LDD
        addr    d,u
        leax    -1,x
        bne     .loop
        tfr     u,d              ; result
        puls    x,y,u,pc
endmod

*************************
module  Sperber_Get_Reply
*************************
        pshs    cc
        orcc    #IRQFLAG|FIRQFLAG
        ldx     #LINEBUF
        ldb     #1              ; RxRDYA
.rloop  bitb    DUART_SRA
        beq     .rloop
        lda     DUART_RHR
        sta     ,x+
        cmpx    #LINEBUF+COLUMNS-2
        beq     .ret
        cmpa    #' '
        bhs     .rloop
.ret    clr     ,x
        puls    cc,pc
endmod

********************
module  Sperber_Sync
*********************
        pshs    cc,x
        orcc    #IRQFLAG|FIRQFLAG
        ldx     #1000           ; time out clock
.wait   lda     #$81
        jsr     Mon_USB_Out
        jsr     Mon_USB_In
        bne     .ret
        leax    -1,x
        bne     .wait
        lda     #2              ; time out
.ret    puls    cc,x,pc
endmod

***************************
* Load File Structure     *
* ----------------------- *
* byte 0   : 'S'          *
* byte 1   : bank         *
* word 2-3 : load address *
* word 4-5 : size         *
* word 6-7 : checksum     *
* 8 - n+7  : data         *
***************************

********************
module  Sperber_Load
**********************
*  ,S : Magic ID     *
* 1,S : Bank         *
* 2,S : Load address *
* 4,S : Payload      *
* 6,S : Checksum     *
**********************
        ldx     #.load
        jsr     Sperber_Send_FN
        bsr     Sperber_Sync
        cmpa    #$81            ; handshake code
        beq     .start
        rts                     ; timeout error

.start  orcc    #IRQFLAG|FIRQFLAG
        jsr     Mon_USB_In      ; ASCII/binary flag
        tfr     a,e
        jsr     Mon_USB_In
        pshs    a
        jsr     Mon_USB_In
        tfr     a,b
        puls    a
        tfr     d,x             ; file size
        leas    -8,s            ; headroom
        tfr     s,u
        ldd     #('S' << 8) | IEP_BANK
        std     ,u
        ldy     #$2000          ; load start
        sty     2,u
        stx     4,u             ; payload

        cmpe    #'0'
        beq     .laba
        lde     #8
.rhloop jsr     Mon_USB_In
        sta     ,u+
        dece
        bne     .rhloop

        ldx     4,s
        ldf     Save_Bank
        ldy     Save_Start
        bne     .dloop          ; use file header

.laba   ldx     4,s             ; payload
        ldy     2,s             ; start
        ldf     1,s             ; bank

.dloop  jsr     Mon_USB_In      ; read data
        jsr     Far_STA
        leax    -1,x
        bne     .dloop
        bsr     Sperber_Report
        andcc   #~(IRQFLAG|FIRQFLAG)

.fine   ldd     2,s             ; start
        addd    4,s             ; payload
        tfr     d,y             ; end address +1
        ldx     2,s             ; start address
        clra                    ; ok
.exit   leas    8,s             ; cleanup stack
        rts
.load   byte    "LOAD",0
endmod

**********************
module  Sperber_Report
**********************
        jsr     Sperber_Sync
        jsr     Sperber_Get_Reply
        ldd     CURLIN
        incd
        bne     .ret            ; no print while running
        ldx     #LINEBUF
        lda     ,x
        sta     MON_REPORT
        cmpa    #'['
        bne     .labb
        lda     #ESC
        CALL    PUTC
        lda     #RED+'0'
        CALL    PUTC

.labb   CALL    PUTS
.ret    rts
endmod

********************
module  Sperber_Save
*********************************
* Input ; X = start address 1,s *
*       : Y = end   address 3,s *
*       : F = bank              *
* Output: A = error # or 0      *
*********************************
        pshs    cc,x,y,u        ; IRQ,start,end+1
        tfr     y,d
        subr    x,d             ; paylod
        tfr     x,y             ; start
        jsr     Mem_Checksum
        tfr     d,u             ; save it
        ldx     #.save
        jsr     Sperber_Send_FN
        jsr     Sperber_Sync
        cmpa    #$81            ; handshake code
        beq     .ok
        ldx     #hsherr
        CALL    PUTS
        lda     #1              ; error
        puls    cc,x,y,u,pc

* write header

.ok     ldd     3,s             ; end
        subd    1,s             ; start
        cmpf    #IEP_BANK
        beq     .len
        addd    #8              ; add header
.len    jsr     Mon_USB_Out     ; length
        tfr     b,a
        jsr     Mon_USB_Out     ; length
        cmpf    #IEP_BANK
        beq     .data

; write header for binary data

        lda     #'S'            ; magic byte
        jsr     Mon_USB_Out
        tfr     f,a             ; bank
        jsr     Mon_USB_Out
        lda     1,s             ; load high
        jsr     Mon_USB_Out
        lda     2,s             ; load low
        jsr     Mon_USB_Out
        ldd     3,s             ; end address
        subd    1,s             ; payload
        jsr     Mon_USB_Out
        tfr     b,a
        jsr     Mon_USB_Out
        tfr     u,d             ; checksum
        jsr     Mon_USB_Out
        tfr     b,a
        jsr     Mon_USB_Out

* write data


.data   ldy     1,s             ; start
        ldx     3,s             ; end
.dloop  jsr     Far_LDA
        jsr     Mon_USB_Out
        cmpr    y,x
        bne     .dloop
        jsr     Sperber_Report
        lda     LINEBUF
        cmpa    #'S'
        bne     .ret
        clra                    ; no error
.ret    puls    cc,x,y,u,pc
.save   byte    "SAVE",0
endmod
hsherr  fcc     "\e2[handshake error]\n",FS,0

