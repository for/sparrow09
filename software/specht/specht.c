#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <time.h>
#define SDL3

#ifdef SDL3
#include <SDL3/SDL.h>
#else
#include <SDL2/SDL.h>
#define SDL_KMOD_LSHIFT    KMOD_LSHIFT
#define SDL_KMOD_RSHIFT    KMOD_RSHIFT
#define SDL_KMOD_LCTRL     KMOD_LCTRL
#define SDL_KMOD_RCTRL     KMOD_RCTRL
#define SDL_KMOD_LALT      KMOD_LALT
#define SDL_KMOD_RALT      KMOD_RALT
#define SDL_EVENT_QUIT     SDL_QUIT
#define SDL_EVENT_KEY_DOWN SDL_KEYDOWN
#endif

#define GERKBD 1
#define SLEN   256

#define MAX64K 0x10000
#define BANKS  8
#define COLUMNS 85

#define ACK 0x06
#define LF  0x0a
#define CR  0x0d
#define ESC 0x1b
#define FS  0x1c

// Flags of the CC register

#define CARRY    1
#define OVERFL   2
#define ZERO     4
#define NEGATIVE 8
#define IRQ     16
#define HALF    32
#define FIRQ    64
#define ENTIRE 128

// Flags of the MD register

#define MD_NATIVE    1
#define MD_STACKING  2
#define MD_ILLEGAL  64
#define MD_DIVIDE  128

int Ren = 0; // 1:hardware, 0:software

typedef unsigned char  BYTE;
typedef unsigned short WORD;
typedef unsigned int   QUAD;

// Accumulators in little endian

union Accu_Union
{
   QUAD Q;
   WORD WR[2];
   BYTE BR[4];
   struct {WORD W,D;};
   struct {BYTE F,E,B,A;};
}  Acc;

// 16 bit registers

union Regi_Union
{
   WORD R[5];
   struct {WORD X,Y,U,S,V;};
}  Reg;

//  8 bit registers

BYTE CC,DP,MD;

BYTE ROM[MAX64K];
BYTE RAM[BANKS][MAX64K];

#define INMAX 16
WORD Trap[INMAX];

void (*Opc[256])(void); // single byte opcodes
void (*O10[256])(void); // $10xx opcodes
void (*O11[256])(void); // $11xx opcodes
void (*PoB[256])(void); // postbye values

#define MAXSYM 3000
#define LENSYM   32

int  Symbols;
int  SymVal[MAXSYM];
char Symbol[MAXSYM][LENSYM];
char SYM_fn[SLEN] = "sparrow.sym";

int Log = 1;
FILE * lf;

char *ROM_fn = "sparrow.rom";
char *SD_fn  = "sparrow.sd";
size_t SD_Size;
BYTE *SD;
BYTE *LBA_Ptr;
char Msg[SLEN];

int  PC;   // program counter
int  Rom;  // ROM switched on or off
int  Bank; // Active RAM bank
BYTE pg;   // page prefix
BYTE oc;   // operation code
BYTE pb;   // post byte
WORD ea;   // effective address

SDL_Window   *EmuWin;
SDL_Surface  *EmuSur;
SDL_Renderer *EmuRen;
SDL_Cursor   *EmuCur;
#ifdef SDL3
SDL_FRect     PixRec;
SDL_FRect     Cur;
#else
SDL_Rect      PixRec;
SDL_Rect      Cur;
#endif

int EmuWin_Width  = 512;
int EmuWin_Height = 212;
int xborder = 1;
int yborder = 1;
int xscale  = 2;
int yscale  = 3;
int WinWidth;
int WinHeight;
int InsideESC;
BYTE  CurVisX;
BYTE  CurVisY;
BYTE *CursorX;
BYTE *CursorY;
BYTE *CursorV;
BYTE *B_Jump;
BYTE *TextColor;
BYTE *BGColor;
BYTE *InsertMode;
BYTE *SD_cb_sec; // cluster sector buffered
WORD  KEYBUF;
WORD  keyrdp;
WORD  keywrp;
BYTE *lastkey;

BYTE *Font_Start;

BYTE Colbit[8] = {0x80,0x40,0x20,0x10,0x08,0x04,0x02,0x01};
BYTE Valbit[8] = {0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x60};
//                   0    1    2    3    4    5    6    7
BYTE R[16]     = {0x00,0xff,0xff,0x00,0x00,0x00,0xff,0xff,
                  0x80,0xc0,0x80,0x00,0x00,0x80,0x80,0x00};
BYTE G[16]     = {0x00,0xff,0x00,0xff,0x00,0xff,0xff,0x00,
                  0x80,0xc0,0x00,0x80,0x00,0x80,0x00,0x80};
BYTE B[16]     = {0x00,0xff,0x00,0x00,0xff,0xff,0x00,0xff,
                  0x80,0xc0,0x00,0x00,0x80,0x00,0x80,0x80};

void Print_Status(FILE *fp)
{
   FILE *wp;
   wp = fopen("specht.ram","wb");
   fwrite(RAM,0x10000,8,wp);
   fclose(wp);

   fprintf(fp,"P=%4.4x:%2.2x %2.2x %2.2x %2.2x\n",PC,
              ROM[PC],ROM[PC+1],ROM[PC+2],ROM[PC+3]);
   fprintf(fp,"D=%4.4x W=%4.4x\n",Acc.D,Acc.W);
   fprintf(fp,"Reg.X=%4.4x Reg.Y=%4.4x\n",Reg.X,Reg.Y);
   fprintf(fp,"Reg.U=%4.4x Reg.S=%4.4x\n",Reg.U,Reg.S);
   fprintf(fp,"Reg.V=%4.4x MD=%2.2x\n",Reg.V,MD);
   fprintf(fp,"DP=%2.2x CC=%2.2x\n",DP,CC);
   fprintf(fp,"Bank=%d\n",Bank);
}

BYTE *Get_Addr(WORD i)
{
   if (Rom && i >= 0x8000) return &ROM[i];
   if (i < 0x2000)         return &RAM[0][i];
                           return &RAM[Bank][i];
}

BYTE Get_Byte(WORD i)
{
   if (i >= 0xff00) return ROM[i];
   if (i >= 0xfe00) fprintf(lf,"Read %4.4x = %2.2x\n",i,ROM[i]);
   if (Rom && i >= 0x8000) return ROM[i];
   if (i < 0x2000)         return RAM[0][i];
                           return RAM[Bank][i];
}

BYTE Prg_Byte(void)
{
   if (PC & 0x8000) return ROM[PC++];
   else             return RAM[0][PC++];
}

WORD Get_Word(WORD i)
{
   return (Get_Byte(i)<<8) | Get_Byte(i+1);
}

BYTE GBI(void)
{
   return Get_Byte(PC++);
}

WORD GWI(void)
{
   return (GBI() << 8) | GBI();
}

WORD Post_Byte(void)
{
   pb = GBI();
   PoB[pb]();
   return ea;
}

BYTE GBD(void)
{
   ea = (DP << 8) | GBI();
   return Get_Byte(ea);
}

WORD GWD(void)
{
   ea = (DP << 8) | GBI();
   return Get_Word(ea);
}

BYTE GBP(void)
{
   ea = Post_Byte();
   return Get_Byte(ea);
}

WORD GWP(void)
{
   ea = Post_Byte();
   return Get_Word(ea);
}

BYTE GBE(void)
{
   ea = GWI();
   return Get_Byte(ea);
}

WORD GWE(void)
{
   ea = GWI();
   return Get_Word(ea);
}

void Put_Byte(BYTE b, WORD a)
{
   if (a >= 0xfe40 && a < 0xfe60) return; // ignore VDP
   if (a >= 0xfe00)
   {
      Print_Status(lf);
      printf("\nI/O access PC=%4.4x\n",PC);
      exit(1);
   }
   if (a < 0x2000) RAM[0][a] = b;
   else            RAM[Bank][a] = b;
}

void Set_Byte_Flags(BYTE b)
{
   CC &= ~(OVERFL | ZERO | NEGATIVE);
   if (b ==   0) CC |= ZERO;
   if (b & 0x80) CC |= NEGATIVE;
}

void Put_Byte_Flags(BYTE b, WORD w)
{
   Put_Byte(b,w);
   Set_Byte_Flags(b);
}

void Put_Word(WORD w, WORD a)
{
   Put_Byte(w >>  8, a  );
   Put_Byte(w  &255, a+1);
}

void Set_Word_Flags(WORD w)
{
   CC &= ~(OVERFL | ZERO | NEGATIVE);
   if (w ==     0) CC |= ZERO;
   if (w & 0x8000) CC |= NEGATIVE;
}

void Put_Word_Flags(WORD v, WORD w)
{
   Put_Word(v,w);
   Set_Word_Flags(v);
}

void Put_Quad(QUAD q, WORD a)
{
   Put_Byte((q >> 24)    , a  );
   Put_Byte((q >> 16)&255, a+1);
   Put_Byte((q >>  8)&255, a+2);
   Put_Byte((q      )&255, a+3);
}

void Clear_Screen(void)
{
   SDL_SetRenderDrawColor(EmuRen, 0, 0, 0, 255);
   SDL_RenderClear(EmuRen);
   SDL_SetRenderDrawColor(EmuRen, 0, 255, 0, 255);
}

void Init_SDL(void)
{
   int i;
#ifdef SDL3
   SDL_Init(SDL_INIT_VIDEO);
#else
   SDL_RendererInfo EmuInfo;
   if (SDL_Init(SDL_INIT_VIDEO) < 0)
   {
      printf("\nFailed to initialize the SDL2 library\n");
      exit(1);
   }
#endif

   WinWidth  = xscale * (EmuWin_Width  + 12 * xborder);
   WinHeight = yscale * (EmuWin_Height + 16 * yborder);
#ifdef SDL3
   EmuWin = SDL_CreateWindow("Specht", WinWidth, WinHeight,0);
#else
   EmuWin = SDL_CreateWindow("Specht",
                             SDL_WINDOWPOS_CENTERED,
                             SDL_WINDOWPOS_CENTERED,
                             WinWidth, WinHeight,0);
#endif
   if (!EmuWin)
   {
      printf("\nFailed to create emulation window\n");
      exit(1);
   }

/*
Render Drivers on MAC Studio
----------------------------
0 metal      - garbage display
1 opengl     - timing issues (may need dleays)
2 opengles2  - not available
3 vulkan     - not available
4 gpu        - segmentation fault
5 software
*/


#ifdef SDL3
   EmuRen = SDL_CreateRenderer(EmuWin,"software");
   fprintf(lf,"\nAvailable Render Drivers\n");
   fprintf(lf,"------------------------\n");
   for (i=0 ; i < SDL_GetNumRenderDrivers() ; ++i)
      fprintf(lf,"%d %s\n",i,SDL_GetRenderDriver(i));
   fprintf(lf,"\n");
#else
   if (Ren) EmuRen = SDL_CreateRenderer(EmuWin,-1,SDL_RENDERER_ACCELERATED);
   else     EmuRen = SDL_CreateRenderer(EmuWin,-1,SDL_RENDERER_SOFTWARE);
   SDL_GetRendererInfo(EmuRen, &EmuInfo);
   fprintf(lf,"Renderer:%s\n",EmuInfo.name);
   fprintf(lf,"%d Render Drivers\n",SDL_GetNumRenderDrivers());
   EmuCur = SDL_CreateSystemCursor(SDL_SYSTEM_CURSOR_IBEAM);
   SDL_SetCursor(EmuCur);
#endif

   if (!EmuRen)
   {
      printf("\nCould not create renderer\n");
      printf("%s\n",SDL_GetError());
      exit(1);
   }
   EmuSur = SDL_GetWindowSurface(EmuWin);
   Clear_Screen();
}

void Set_Text_Color(BYTE c)
{
   if (c > 64) c -= 55; // A-F
               c &= 15; // 0-9
   SDL_SetRenderDrawColor(EmuRen,R[c],G[c],B[c],255);
}

// ***********************
// * Acc.A = character   *
// * Acc.B = color BG/FG *
// * Acc.E = row         *
// * Acc.F = column      *
// ***********************

void VDP_Sync_Char(void)
{
   BYTE *Scan;
   int  Col,Row,x;
#ifdef SDL3
   SDL_FRect Pix;
#else
   SDL_Rect Pix;
#endif

   Scan = Font_Start + Acc.A * 8;
   Pix.w = xscale * 6;
   Pix.h = yscale * 8;
   Pix.y = (Acc.E + yborder) * 8 * yscale;
   Pix.x = (Acc.F + xborder) * 6 * xscale;

   if (Acc.E == CurVisY && Acc.F == CurVisX) Cur.w = 0;

   Set_Text_Color(Acc.B >> 4);      // background colour
   SDL_RenderFillRect(EmuRen,&Pix); // fill cell with BG
   if (Acc.A == 0x20)
   {
      SDL_RenderPresent(EmuRen);
      return;       // ready if blank
   }
   Pix.w = xscale;
   Pix.h = yscale;
   x = Pix.x;
   Set_Text_Color(Acc.B & 15);
   for (Row=0 ; Row < 8 ; ++Row)
   {
      Pix.x = x;
      for (Col=0 ; Col < 6 ; ++Col)
      {
         if (*Scan & Colbit[Col])
         {
            SDL_RenderFillRect(EmuRen,&Pix);
         }
         Pix.x += xscale;
      }
      ++Scan;
      Pix.y += yscale;
   }
   SDL_RenderPresent(EmuRen);
   if (Ren) SDL_Delay(10);
}

void Cursor_Off(void)
{
   QUAD Save;
   WORD ad;
   BYTE ch,co;
   if (Cur.w == 0) return;
   Save = Acc.Q;
   Acc.E = CurVisY;
   Acc.F = CurVisX;
   ad = 0x6000 + 2 * (CurVisX + CurVisY * COLUMNS);
   Acc.A = RAM[7][ad];
   Acc.B = RAM[7][ad+1];
   VDP_Sync_Char();
   Acc.Q = Save;
}

void Cursor_On(void)
{
   if (Cur.w) Cursor_Off();
   if (!*CursorV) return;
   CurVisX = *CursorX;
   CurVisY = *CursorY;
   Cur.w = xscale * 6;
   Cur.h = yscale;
   Cur.y = ((CurVisY+yborder) * 8 + 7) * yscale;
   Cur.x = ((CurVisX+xborder) * 6    ) * xscale;
   if (*InsertMode)
   {
      Cur.h += yscale;
      Cur.y -= yscale;
   }
   Set_Text_Color(*TextColor);
   SDL_RenderFillRect(EmuRen,&Cur);
   SDL_RenderPresent(EmuRen);
}

void Todo_Op(void)
{
   if (pg == 0x10 || pg == 0x11)
   sprintf(Msg,"Not implemented code %2.2x%2.2x at %4.4x",pg,oc,PC-2);
   else
   sprintf(Msg,"Not implemented code %2.2x at %4.4x",oc,PC-1);
   SDL_ShowSimpleMessageBox(0, "Specht Box", Msg, NULL);
   Print_Status(lf);
   exit(1);
}

void Todo_PB(void)
{
   sprintf(Msg,"Not implemented postbye code %2.2x at %4.4x",pb,PC-1);
   SDL_ShowSimpleMessageBox(0, "Specht Box", Msg, NULL);
   Print_Status(lf);
   exit(1);
}

void PoB_PINC(void)
{
   int ri = (pb >> 5) & 3;
   ea = Reg.R[ri];
   Reg.R[ri] += 1 + (pb & 1);
   if (pb & 0x10) ea = Get_Word(ea);
}

void PoB_PDEC(void)
{
   int ri = (pb >> 5) & 3;
   Reg.R[ri] -= 1 + (pb & 1);
   ea = Reg.R[ri];
   if (pb & 0x10) ea = Get_Word(ea);
}

void PoB_REG(void)
{
   ea = Reg.R[(pb >> 5) & 3];
   if (pb & 0x10) ea = Get_Word(ea);
}

void PoB_OF8(void)
{
   int ri;
   ri = (pb >> 5) & 3;
   ea = (char)GBI() + Reg.R[ri];
   if (pb & 0x10) ea = Get_Word(ea);
}

void PoB_PC8(void)
{
   ea = PC+1;
   ea += (char)GBI();
   if (pb & 0x10) ea = Get_Word(ea);
}

void PoB_P16(void)
{
   ea = (short)GWI() + PC;
   if (pb & 0x10) ea = Get_Word(ea);
}

void PoB_O16(void)
{
   int ri;
   ri = (pb >> 5) & 3;
   ea = (short)GWI() + Reg.R[ri];
   if (pb & 0x10) ea = Get_Word(ea);
}

void PoB_AR(void)
{
   int ri = (pb >> 5) & 3;
   ea = (char)Acc.A + Reg.R[ri];
   if (pb & 0x10) ea = Get_Word(ea);
}

void PoB_BR(void)
{
   int ri = (pb >> 5) & 3;
   ea = (char)Acc.B + Reg.R[ri];
   if (pb & 0x10) ea = Get_Word(ea);
}

void PoB_DR(void)
{
   int ri = (pb >> 5) & 3;
   ea = (short)Acc.D + Reg.R[ri];
   if (pb & 0x10) ea = Get_Word(ea);
}

void PoB_WR(void)
{
   int ri = (pb >> 5) & 3;
   ea = (short)Acc.W + Reg.R[ri];
   if (pb & 0x10) ea = Get_Word(ea);
}

void PoB_ER(void)
{
   int ri = (pb >> 5) & 3;
   ea = (char)Acc.E + Reg.R[ri];
   if (pb & 0x10) ea = Get_Word(ea);
}

void PoB_FR(void)
{
   int ri = (pb >> 5) & 3;
   ea = (char)Acc.F + Reg.R[ri];
   if (pb & 0x10) ea = Get_Word(ea);
}

void PoB_IND(void)
{
   ea = GWI();
   ea = Get_Word(ea);
}

void PoB_Offset(void)
{
   int ri = (pb >> 5) & 3;
   int of = pb & 0x1f;
   ea = Reg.R[ri] + of;
   if (of > 15) ea -= 32;
}

void PoB_W(void)   { ea = Acc.W;if (pb & 0x10) ea = Get_Word(ea); }
void PoB_WOW(void)
{
   ea = (short)GWI() + Acc.W;
   if (pb & 0x10) ea = Get_Word(ea);
}
void PoB_WPL(void) { ea = Acc.W; Acc.W += 2;if (pb & 0x10) ea = Get_Word(ea); }
void PoB_MIW(void) { Acc.W -= 2; ea = Acc.W;if (pb & 0x10) ea = Get_Word(ea); }

void Set_Quad_Flags(QUAD q)
{
   CC &= ~(OVERFL | ZERO | NEGATIVE);
   if (q ==         0) CC |= ZERO;
   if (q & 0x80000000) CC |= NEGATIVE;
}

BYTE Sub_Byte_Flags(BYTE a, BYTE b)
{
   BYTE d = a - b;
   CC &= ~(NEGATIVE | ZERO | OVERFL | CARRY);
   if (d & 0x80) CC |= NEGATIVE;
   if (d  ==  0) CC |= ZERO;
   if (a  <   b) CC |= CARRY;
   if (((a&0x80)^(b&0x80)) && ((a&0x80)^(d&0x80))) CC |= OVERFL;
   return d;
}

BYTE Sbc_Byte_Flags(BYTE a, BYTE b)
{
   short d = a - b - (CC & CARRY);;
   CC &= ~(NEGATIVE | ZERO | OVERFL | CARRY);
   if (d & 0x80) CC |= NEGATIVE;
   if (d  ==  0) CC |= ZERO;
   if (d  <   0) CC |= CARRY;
   if (((a&0x80)^(b&0x80)) && ((a&0x80)^(d&0x80))) CC |= OVERFL;
   return d;
}

WORD Sub_Word_Flags(WORD a, WORD b)
{
   WORD d = a - b;
   CC &= ~(NEGATIVE | ZERO | OVERFL | CARRY);
   if (d & 0x8000) CC |= NEGATIVE;
   if (d   ==   0) CC |= ZERO;
   if (a   <    b) CC |= CARRY;
   if (((a&0x8000)^(b&0x8000)) && ((a&0x8000)^(d&0x8000))) CC |= OVERFL;
   return d;
}

WORD Sbc_Word_Flags(WORD a, WORD b)
{
   int d = a - b - (CC & CARRY);
   CC &= ~(NEGATIVE | ZERO | OVERFL | CARRY);
   if (d & 0x8000) CC |= NEGATIVE;
   if (d   ==   0) CC |= ZERO;
   if (d   <    0) CC |= CARRY;
   if (((a&0x8000)^(b&0x8000)) && ((a&0x8000)^(d&0x8000))) CC |= OVERFL;
   return d;
}

BYTE Add_Byte_Flags(BYTE a, BYTE b)
{
   WORD s = a + b;
   CC &= ~(HALF | NEGATIVE | ZERO | OVERFL | CARRY);
   if (s > 0xff)
   {
      s &= 0xff;
      CC |= CARRY;
   }
   if (s & 0x80) CC |= NEGATIVE;
   if (s  ==  0) CC |= ZERO;
   if (!((a&0x80)^(b&0x80)) && ((a&0x80)^(s&0x80))) CC |= OVERFL;
   return s;
}

BYTE Adc_Byte_Flags(BYTE a, BYTE b)
{
   WORD s = a + b + (CC & CARRY);
   CC &= ~(HALF | NEGATIVE | ZERO | OVERFL | CARRY);
   if (s > 0xff)
   {
      s &= 0xff;
      CC |= CARRY;
   }
   if (s & 0x80) CC |= NEGATIVE;
   if (s  ==  0) CC |= ZERO;
   if (!((a&0x80)^(b&0x80)) && ((a&0x80)^(s&0x80))) CC |= OVERFL;
   return s;
}

WORD Add_Word_Flags(WORD a, WORD b)
{
   unsigned int s = a + b;
   CC &= ~(HALF | NEGATIVE | ZERO | OVERFL | CARRY);
   if (s > 0xffff)
   {
      s &= 0xffff;
      CC |= CARRY;
   }
   if (s & 0x8000) CC |= NEGATIVE;
   if (s   ==   0) CC |= ZERO;
   if (!((a&0x8000)^(b&0x8000)) && ((a&0x8000)^(s&0x8000))) CC |= OVERFL;
   return s;
}

WORD Adc_Word_Flags(WORD a, WORD b)
{
   unsigned int s = a + b + (CC & CARRY);
   CC &= ~(HALF | NEGATIVE | ZERO | OVERFL | CARRY);
   if (s > 0xffff)
   {
      s &= 0xffff;
      CC |= CARRY;
   }
   if (s & 0x8000) CC |= NEGATIVE;
   if (s   ==   0) CC |= ZERO;
   if (!((a&0x8000)^(b&0x8000)) && ((a&0x8000)^(s&0x8000))) CC |= OVERFL;
   return s;
}

int Get_Reg_Val(BYTE r)
{
   switch (r)
   {
      case  0: return Acc.D;
      case  1: return Reg.X;
      case  2: return Reg.Y;
      case  3: return Reg.U;
      case  4: return Reg.S;
      case  5: return    PC;
      case  6: return Acc.W;
      case  7: return Reg.V;
      case  8: return Acc.A;
      case  9: return Acc.B;
      case 10: return    CC;
      case 11: return    DP;
      case 12: return     0;
      case 13: return     0;
      case 14: return Acc.E;
      case 15: return Acc.F;
   }
   return 0;
}

void Put_Reg_Val(int v, BYTE r)
{
   switch (r)
   {
      case  0: Acc.D = v; break;
      case  1: Reg.X = v; break;
      case  2: Reg.Y = v; break;
      case  3: Reg.U = v; break;
      case  4: Reg.S = v; break;
      case  5:    PC = v; break;
      case  6: Acc.W = v; break;
      case  7: Reg.V = v; break;
      case  8: Acc.A = v; break;
      case  9: Acc.B = v; break;
      case 10:    CC = v; break;
      case 11:    DP = v; break;
      case 14: Acc.E = v; break;
      case 15: Acc.F = v; break;
   }
}

void Set_CC0(BYTE B)
{
   CC = (CC & 0xfe) | (B & 1);
}

void Set_CC7(BYTE B)
{
   CC = (CC & 0xfe) | (B >> 7);
}

void Opc_00_NEG_D(void)
{
   char V = GBD();
   if (V == -128) CC |= OVERFL;
   else           Put_Byte_Flags(-V,ea);
}

void Opc_01_OIM_D(void) {Put_Byte_Flags(GBI()|GBD(),ea);}
void Opc_02_AIM_D(void) {Put_Byte_Flags(GBI()&GBD(),ea);}
void Opc_03_COM_D(void) {Put_Byte_Flags(~GBD(),ea);CC|=CARRY;}

void Opc_04_LSR_D(void)
{
   BYTE V = GBD();
   Set_CC0(V);
   Put_Byte_Flags(V>>1,ea);
}

void Opc_05_EIM_D(void) {Put_Byte_Flags(GBI()^GBD(),ea);}

void Opc_06_ROR_D(void)
{
   BYTE V = GBD();
   BYTE C = V & 1;
   V >>= 1;
   if (CC & CARRY) V |= 0x80;
   Put_Byte_Flags(V,ea);
   Set_CC0(C);
}

void Opc_07_ASR_D(void)
{
   BYTE V = GBD();
   Set_CC0(V);
   V >>= 1;
   if (V & 0x40) V |= 0x80;
   Put_Byte_Flags(V,ea);
}

void Opc_08_LSL_D(void)
{
   BYTE V = GBD();
   Set_CC7(V);
   Put_Byte_Flags(V<<1,ea);
}

void Opc_09_ROL_D(void)
{
   BYTE V = GBD();
   BYTE C = V & 0x80;
   V <<= 1;
   if (CC & CARRY) V |= 1;
   Put_Byte_Flags(V,ea);
   Set_CC7(C);
}

void Opc_0A_DEC_D(void)
{
   BYTE V = GBD() - 1;
   Put_Byte_Flags(V,ea);
   if (V == 0x7f) CC |= OVERFL;
}

void Opc_0B_TIM_D(void)
{
   Set_Byte_Flags(GBI() & GBD());
}

void Opc_0C_INC_D(void)
{
   BYTE V = GBD() + 1;
   Put_Byte_Flags(V,ea);
   if (V == 0x80) CC |= OVERFL;
}

void Opc_0D_TST_D(void) {Set_Byte_Flags(GBD());}
void Opc_0E_JMP_D(void) {PC = GWD();}
void Opc_0F_CLR_D(void) {GBD();Put_Byte_Flags(0,ea);CC&=~CARRY;}
void Opc_12_NOP(void) {}

void Opc_14_SEXW(void)
{
   if (Acc.W & 0x8000) Acc.D = 0xffff;
   else                Acc.D = 0;
   Set_Quad_Flags(Acc.Q);
}

//  7      6      5      4  3  2  1  0
// PC  Reg.U  Reg.Y  Reg.X DP  B  A CC

void Push_All(void)
{
   Reg.S-=2; Put_Word(PC   ,Reg.S);
   Reg.S-=2; Put_Word(Reg.U,Reg.S);
   Reg.S-=2; Put_Word(Reg.Y,Reg.S);
   Reg.S-=2; Put_Word(Reg.X,Reg.S);
   Reg.S-- ; Put_Byte(DP   ,Reg.S);
   Reg.S-=2; Put_Word(Acc.W,Reg.S);
   Reg.S-=2; Put_Word(Acc.D,Reg.S);
   Reg.S-- ; Put_Byte(CC   ,Reg.S);
}

void Pull_All(void)
{
   CC    = Get_Byte(Reg.S); Reg.S++ ;
   Acc.D = Get_Word(Reg.S); Reg.S+=2;
   Acc.W = Get_Word(Reg.S); Reg.S+=2;
   DP    = Get_Byte(Reg.S); Reg.S++ ;
   Reg.X = Get_Word(Reg.S); Reg.S+=2;
   Reg.Y = Get_Word(Reg.S); Reg.S+=2;
   Reg.U = Get_Word(Reg.S); Reg.S+=2;
   PC    = Get_Word(Reg.S); Reg.S+=2;
}

void Opc_15_MON(void)   {Push_All(); PC = Get_Word(0xfff0);}
void Opc_16_LBRA(void)  {PC += (signed short)GWI();}

void Opc_17_LBSR(void)
{
   signed short Rel = GWI();
   RAM[0][--Reg.S] = PC & 255;
   RAM[0][--Reg.S] = PC >>  8;
   PC += Rel;
}

void Opc_19_DAA(void)
{
   BYTE H = Acc.A >> 4;
   BYTE L = Acc.A & 15;
   if ((CC & CARRY) || H > 9 || (H > 8 && L > 9)) Acc.A += 0x60;
   if ((CC & HALF ) || L > 9)                     Acc.A += 0x06;
   Set_Byte_Flags(Acc.A);
}

void Opc_1A_ORCC(void)  { CC |= GBI(); }
void Opc_1C_ANDCC(void) { CC &= GBI(); }

void Opc_1D_SEX(void)
{
   if (Acc.B & 0x80) Acc.A = 0xff;
   else              Acc.A = 0;
   Set_Word_Flags(Acc.D);
}

void Opc_1E_EXG(void)
{
   BYTE rs,rd;
   WORD vs,vd;
   pb = GBI();
   rs = pb >> 4;
   rd = pb & 15;
   vs = Get_Reg_Val(rs);
   vd = Get_Reg_Val(rd);
   if (rs > 7 && rd < 8) vs |= vs << 8; //  8 bit -> 16 bit
   if (rs < 8 && rd > 7)                // 16 bit ->  8 bit
   {
      if (rd == 8 || rd == 11 || rd == 14) vs >>=   8; // A,DP,E
      else                                 vs  &= 255; // B,CC,F
   }
   Put_Reg_Val(vs,rd);
   if (rd > 7 && rs < 8) vd |= vd << 8; //  8 bit -> 16 bit
   if (rd < 8 && rs > 7)                // 16 bit ->  8 bit
   {
      if (rs == 8 || rs == 11 || rs == 14) vd >>=   8; // A,DP,E
      else                                 vd  &= 255; // B,CC,F
   }
   Put_Reg_Val(vd,rs);
}

void Opc_1F_TFR(void)
{
   BYTE rs,rd;
   int  va;
   pb = GBI();
   rs = pb >> 4;
   rd = pb & 15;
   va = Get_Reg_Val(rs);
   if (rs > 7 && rd < 8) va |= va << 8; //  8 bit -> 16 bit
   if (rs < 8 && rd > 7)                // 16 bit ->  8 bit
   {
      if (rd == 8 || rd == 11 || rd == 14) va >>=   8; // A,DP,E
      else                                 va  &= 255; // B,CC,F
   }
   Put_Reg_Val(va,rd);
}

void Opc_20_BRA(void) {char R=GBI(); PC+=R;}
void Opc_21_BRN(void) {++PC;}
void Opc_22_BHI(void) {char R=GBI();if (!(CC & (ZERO | CARRY))) PC+=R;}
void Opc_23_BLS(void) {char R=GBI();if   (CC & (ZERO | CARRY))  PC+=R;}
void Opc_24_BCC(void) {char R=GBI();if (!(CC & CARRY))    PC+=R;}
void Opc_25_BCS(void) {char R=GBI();if   (CC & CARRY)     PC+=R;}
void Opc_26_BNE(void) {char R=GBI();if (!(CC & ZERO))     PC+=R;}
void Opc_27_BEQ(void) {char R=GBI();if   (CC & ZERO)      PC+=R;}
void Opc_28_BVC(void) {char R=GBI();if (!(CC & OVERFL))   PC+=R;}
void Opc_29_BVS(void) {char R=GBI();if   (CC & OVERFL)    PC+=R;}
void Opc_2A_BPL(void) {char R=GBI();if (!(CC & NEGATIVE)) PC+=R;}
void Opc_2B_BMI(void) {char R=GBI();if   (CC & NEGATIVE)  PC+=R;}

void Opc_2C_BGE(void)
{
   BYTE N,V;
   char Rel = GBI();
   N = (CC & NEGATIVE) != 0;
   V = (CC & OVERFL  ) != 0;
   if (N == V) PC += Rel;
}

void Opc_2D_BLT(void)
{
   BYTE N,O;
   char Rel = GBI();
   N = (CC & NEGATIVE) != 0;
   O = (CC & OVERFL)   != 0;
   if (N != O) PC += Rel;
}

void Opc_2E_BGT(void)
{
   BYTE N,O,Z;
   char Rel = GBI();
   N = (CC & NEGATIVE) != 0;
   O = (CC & OVERFL)   != 0;
   Z = (CC & ZERO)     != 0;
   if (!Z && N == O) PC += Rel;
}

void Opc_2F_BLE(void)
{
   BYTE N,O,Z;
   char Rel = GBI();
   N = (CC & NEGATIVE) != 0;
   O = (CC & OVERFL)   != 0;
   Z = (CC & ZERO)     != 0;
   if (Z || N != O) PC += Rel;
}

void Opc_30_LEAX(void)
{
   Reg.X = Post_Byte();
   if (Reg.X) CC &= ~ZERO;
   else       CC |=  ZERO;
}

void Opc_31_LEAY(void)
{
   Reg.Y = Post_Byte();
   if (Reg.Y) CC &= ~ZERO;
   else       CC |=  ZERO;
}

void Opc_32_LEAS(void) { Reg.S = Post_Byte(); }
void Opc_33_LEAU(void) { Reg.U = Post_Byte(); }

void Opc_34_PSHS(void)
{
   BYTE R;
   int  i;
   R = GBI();

   for (i=0 ; i < 8 ; ++i)
   {
      if (Colbit[i] & R)
      {
         switch (i)
         {
            case 7: Put_Byte(CC    ,--Reg.S); break;
            case 6: Put_Byte(Acc.A ,--Reg.S); break;
            case 5: Put_Byte(Acc.B ,--Reg.S); break;
            case 4: Put_Byte(DP    ,--Reg.S); break;
            case 3: Reg.S-=2; Put_Word(Reg.X ,Reg.S); break;
            case 2: Reg.S-=2; Put_Word(Reg.Y ,Reg.S); break;
            case 1: Reg.S-=2; Put_Word(Reg.U ,Reg.S); break;
            case 0: Reg.S-=2; Put_Word(PC,Reg.S); break;
         }
      }
   }
}

void Opc_35_PULS(void)
{
   BYTE R;
   int  i;
   R = GBI();

   for (i=7 ; i >= 0 ; --i)
   {
      if (Colbit[i] & R)
      {
         switch (i)
         {
            case 7: CC    = Get_Byte(Reg.S++); break;
            case 6: Acc.A = Get_Byte(Reg.S++); break;
            case 5: Acc.B = Get_Byte(Reg.S++); break;
            case 4: DP    = Get_Byte(Reg.S++); break;
            case 3: Reg.X = Get_Word(Reg.S); Reg.S+=2; break;
            case 2: Reg.Y = Get_Word(Reg.S); Reg.S+=2; break;
            case 1: Reg.U = Get_Word(Reg.S); Reg.S+=2; break;
            case 0: PC    = Get_Word(Reg.S); Reg.S+=2; break;
         }
      }
   }
}

void Opc_36_PSHU(void)
{
   BYTE R;
   int  i;
   R = GBI();

   for (i=0 ; i < 8 ; ++i)
   {
      if (Colbit[i] & R)
      {
         switch (i)
         {
            case 7: Put_Byte(CC    ,--Reg.U); break;
            case 6: Put_Byte(Acc.A ,--Reg.U); break;
            case 5: Put_Byte(Acc.B ,--Reg.U); break;
            case 4: Put_Byte(DP    ,--Reg.U); break;
            case 3: Reg.U-=2; Put_Word(Reg.X ,Reg.U); break;
            case 2: Reg.U-=2; Put_Word(Reg.Y ,Reg.U); break;
            case 1: Reg.U-=2; Put_Word(Reg.S ,Reg.U); break;
            case 0: Reg.U-=2; Put_Word(PC,Reg.U); break;
         }
      }
   }
}

void Opc_37_PULU(void)
{
   BYTE R;
   int  i;
   R = GBI();

   for (i=7 ; i >= 0 ; --i)
   {
      if (Colbit[i] & R)
      {
         switch (i)
         {
            case 7: CC    = Get_Byte(Reg.U++); break;
            case 6: Acc.A = Get_Byte(Reg.U++); break;
            case 5: Acc.B = Get_Byte(Reg.U++); break;
            case 4: DP    = Get_Byte(Reg.U++); break;
            case 3: Reg.X = Get_Word(Reg.U); Reg.U+=2; break;
            case 2: Reg.Y = Get_Word(Reg.U); Reg.U+=2; break;
            case 1: Reg.S = Get_Word(Reg.U); Reg.U+=2; break;
            case 0: PC    = Get_Word(Reg.U); Reg.U+=2; break;
         }
      }
   }
}

void Opc_39_RTS(void) { PC = Get_Word(Reg.S); Reg.S += 2; }
void Opc_3A_ABX(void) { Reg.X += Acc.B; }
void Opc_3B_RTI(void) { Pull_All(); }

void Opc_3D_MUL(void)
{
   Acc.D = Acc.A * Acc.B;
   if (Acc.D == 0)   CC |=  ZERO;
   else              CC &= ~ZERO;
   if (Acc.B & 0x80) CC |=  CARRY;
   else              CC &= ~CARRY;
}

void Opc_3F_SWI(void) { PC = Get_Word(0xfffa); }

void Opc_40_NEGA(void)
{
   if (Acc.A == 0x80) CC |= OVERFL;
   else
   {
      Acc.A = -Acc.A;
      Set_Byte_Flags(Acc.A);
   }
}

void Opc_43_COMA(void)
{
   Acc.A = ~Acc.A;
   Set_Byte_Flags(Acc.A);
   CC |= CARRY;
}

void Opc_44_LSRA(void)
{
   if (Acc.A & 1) CC |=  CARRY;
   else           CC &= ~CARRY;
   Acc.A >>= 1;
   Set_Byte_Flags(Acc.A);
}

void Opc_46_RORA(void)
{
   BYTE C = CC & CARRY;
   if (Acc.A & 1) CC |=  CARRY;
   else           CC &= ~CARRY;
   Acc.A >>= 1;
   if (C) Acc.A |= 0x80;
   Set_Byte_Flags(Acc.A);
}

void Opc_47_ASRA(void)
{
   BYTE C = Acc.A & 0x80;
   if (Acc.A & 1) CC |=  CARRY;
   else           CC &= ~CARRY;
   Acc.A >>= 1;
   Acc.A |= C;
   Set_Byte_Flags(Acc.A);
}

void Opc_48_ASLA(void)
{
   if (Acc.A & 0x80) CC |=  CARRY;
   else              CC &= ~CARRY;
   Acc.A <<= 1;
   Set_Byte_Flags(Acc.A);
}

void Opc_49_ROLA(void)
{
   BYTE C = CC & CARRY;
   if (Acc.A & 0x80) CC |=  CARRY;
   else              CC &= ~CARRY;
   Acc.A <<= 1;
   if (C) Acc.A |= 1;
   Set_Byte_Flags(Acc.A);
}

void Opc_4A_DECA(void) { Set_Byte_Flags(--Acc.A);if (Acc.A==0x7f) CC|= OVERFL;}
void Opc_4C_INCA(void) { Set_Byte_Flags(++Acc.A);if (Acc.A==0x80) CC|= OVERFL;}
void Opc_4D_TSTA(void) { Set_Byte_Flags(Acc.A); }
void Opc_4F_CLRA(void) { Acc.A = 0; CC = (CC & 0xf0) | ZERO; }

// *************
// Accumulator B
// *************

void Opc_50_NEGB(void)
{
   if (Acc.B == 0x80) CC |= OVERFL;
   else
   {
      Acc.B = -Acc.B;
      Set_Byte_Flags(Acc.B);
   }
}

void Opc_53_COMB(void)
{
   Acc.B = ~Acc.B;
   Set_Byte_Flags(Acc.B);
   CC |= CARRY;
}

void Opc_54_LSRB(void)
{
   if (Acc.B & 1) CC |=  CARRY;
   else           CC &= ~CARRY;
   Acc.B >>= 1;
   Set_Byte_Flags(Acc.B);
}

void Opc_56_RORB(void)
{
   BYTE C = CC & CARRY;
   if (Acc.B & 1) CC |=  CARRY;
   else           CC &= ~CARRY;
   Acc.B >>= 1;
   if (C) Acc.B |= 0x80;
   Set_Byte_Flags(Acc.B);
}

void Opc_57_ASRB(void)
{
   BYTE C = Acc.B & 0x80;
   if (Acc.B & 1) CC |=  CARRY;
   else           CC &= ~CARRY;
   Acc.B >>= 1;
   Acc.B |= C;
   Set_Byte_Flags(Acc.B);
}

void Opc_58_ASLB(void)
{
   if (Acc.B & 0x80) CC |=  CARRY;
   else              CC &= ~CARRY;
   Acc.B <<= 1;
   Set_Byte_Flags(Acc.B);
}

void Opc_59_ROLB(void)
{
   BYTE C = CC & CARRY;
   if (Acc.B & 0x80) CC |=  CARRY;
   else              CC &= ~CARRY;
   Acc.B <<= 1;
   Acc.B  |= C;
   Set_Byte_Flags(Acc.B);
}

void Opc_5A_DECB(void) {Set_Byte_Flags(--Acc.B);if (Acc.B==0x7f) CC|=OVERFL;}
void Opc_5C_INCB(void) {Set_Byte_Flags(++Acc.B);if (Acc.B==0x80) CC|=OVERFL;}
void Opc_5D_TSTB(void) {Set_Byte_Flags(Acc.B); }
void Opc_5F_CLRB(void) {Acc.B = 0; CC = (CC & 0xf0) | ZERO; }

// **************************************
// Read-Modify-Write Instructions Indexed
// **************************************

void Opc_60_NEG_P(void)
{
   char V = GBP();
   if (V == -128) CC |= OVERFL;
   else           Put_Byte_Flags(-V,ea);
}

void Opc_61_OIM_P(void)
{
   BYTE I = GBI();
   BYTE V = GBP() | I;
   Put_Byte_Flags(V,ea);
}

void Opc_62_AIM_P(void)
{
   BYTE I = GBI();
   BYTE V = GBP() & I;
   Put_Byte_Flags(V,ea);
}

void Opc_63_COM_P(void)
{
   BYTE V = ~GBP();
   Put_Byte_Flags(V,ea);
   CC |= CARRY;
}

void Opc_64_LSR_P(void)
{
   BYTE V = GBP();
   if (V & 1) CC |=  CARRY;
   else       CC &= ~CARRY;
   Put_Byte_Flags(V>>1,ea);
}

void Opc_65_EIM_P(void)
{
   BYTE I = GBI();
   BYTE V = I ^ GBP();
   Put_Byte_Flags(V,ea);
}

void Opc_66_ROR_P(void)
{
   BYTE V = GBP();
   BYTE C = V & 1;
   V >>= 1;
   if (CC & CARRY) V |= 0x80;
   Put_Byte_Flags(V,ea);
   if (C) CC |=  CARRY;
   else   CC &= ~CARRY;
}

void Opc_67_ASR_P(void)
{
   BYTE V = GBP();
   BYTE C = V & 1;
   V >>= 1;
   if (V & 0x40) V |= 0x80;
   Put_Byte_Flags(V,ea);
   if (C) CC |=  CARRY;
   else   CC &= ~CARRY;
}

void Opc_68_LSL_P(void)
{
   BYTE V = GBP();
   if (V & 0x80) CC |=  CARRY;
   else          CC &= ~CARRY;
   Put_Byte_Flags(V<<1,ea);
}

void Opc_69_ROL_P(void)
{
   BYTE V = GBP();
   BYTE C = V & 0x80;
   V = V << 1 | (CC & CARRY);
   Put_Byte_Flags(V,ea);
   if (C) CC |=  CARRY;
   else   CC &= ~CARRY;
}

void Opc_6A_DEC_P(void)
{
   BYTE V = GBP() - 1;
   Put_Byte_Flags(V,ea);
   if (V == 0x7f) CC |= OVERFL;
}

void Opc_6B_TIM_P(void)
{
   Set_Byte_Flags(GBI() & GBP());
}

void Opc_6C_INC_P(void)
{
   BYTE V = GBP() + 1;
   Put_Byte_Flags(V,ea);
   if (V == 0x80) CC |= OVERFL;
}

void Opc_6D_TST_P(void) {Set_Byte_Flags(GBP()); }
void Opc_6E_JMP_P(void) {PC = Post_Byte(); }
void Opc_6F_CLR_P(void) {Put_Byte(0,Post_Byte()); CC = (CC & 0xf0) | ZERO; }

// ***************************************
// Read-Modify-Write Instructions Extended
// ***************************************

void Opc_70_NEG_E(void)
{
   char V = GBE();
   if (V == -128) CC |= OVERFL;
   else           Put_Byte_Flags(-V,ea);
}

void Opc_71_OIM_E(void)
{
   Put_Byte_Flags(GBI() | GBE(),ea);
}

void Opc_72_AIM_E(void)
{
   Put_Byte_Flags(GBI() & GBE(),ea);
}

void Opc_73_COM_E(void)
{
   Put_Byte_Flags(~GBE(),ea);
   CC |= CARRY;
}

void Opc_74_LSR_E(void)
{
   BYTE V = GBE();
   if (V & 1) CC |=  CARRY;
   else       CC &= ~CARRY;
   Put_Byte_Flags(V>>1,ea);
}

void Opc_75_EIM_E(void)
{
   Put_Byte_Flags(GBI() ^ GBE(),ea);
}

void Opc_76_ROR_E(void)
{
   BYTE V = GBE();
   BYTE C = V & 1;
   V >>= 1;
   if (CC & CARRY) V |= 0x80;
   Put_Byte_Flags(V,ea);
   if (C) CC |=  CARRY;
   else   CC &= ~CARRY;
}

void Opc_77_ASR_E(void)
{
   BYTE V = GBE();
   BYTE C = V & 1;
   V >>= 1;
   if (V & 0x40) V |= 0x80;
   Put_Byte_Flags(V,ea);
   if (C) CC |=  CARRY;
   else   CC &= ~CARRY;
}

void Opc_78_LSL_E(void)
{
   BYTE V = GBE();
   if (V & 0x80) CC |=  CARRY;
   else          CC &= ~CARRY;
   Put_Byte_Flags(V<<1,ea);
}

void Opc_79_ROL_E(void)
{
   BYTE V = GBE();
   BYTE C = V & 0x80;
   V = V << 1 | (CC & CARRY);
   Put_Byte_Flags(V,ea);
   if (C) CC |=  CARRY;
   else   CC &= ~CARRY;
}

void Opc_7A_DEC_E(void)
{
   BYTE V = GBE() - 1;
   Put_Byte_Flags(V,ea);
   if (V == 0x7f) CC |= OVERFL;
}

void Opc_7B_TIM_E(void)
{
   Set_Byte_Flags(GBI() & GBE());
}

void Opc_7C_INC_E(void)
{
   BYTE V = GBE() + 1;
   Put_Byte_Flags(V,ea);
   if (V == 0x80) CC |= OVERFL;
}

void Opc_7D_TST_E(void) {Set_Byte_Flags(GBE());}
void Opc_7E_JMP_E(void) {PC = GWI();}
void Opc_7F_CLR_E(void) {Put_Byte(0,GWI());CC=(CC&0xf0)|ZERO;}

// *******************
// Immediate A,D,X,BSR
// *******************

void Opc_80_SUBA_I(void){Acc.A = Sub_Byte_Flags(Acc.A,GBI());}
void Opc_81_CMPA_I(void){        Sub_Byte_Flags(Acc.A,GBI());}
void Opc_82_SBCA_I(void){Acc.A = Sbc_Byte_Flags(Acc.A,GBI());}
void Opc_83_SUBD_I(void){Acc.D = Sub_Word_Flags(Acc.D,GWI());}
void Opc_84_ANDA_I(void){Set_Byte_Flags(Acc.A &= GBI());}
void Opc_85_BITA_I(void){Set_Byte_Flags(Acc.A &  GBI());}
void Opc_86_LDA_I(void) {Set_Byte_Flags(Acc.A  = GBI());}
void Opc_88_EORA_I(void){Set_Byte_Flags(Acc.A ^= GBI());}
void Opc_89_ADCA_I(void){Acc.A = Adc_Byte_Flags(Acc.A,GBI());}
void Opc_8A_ORA_I(void) {Set_Byte_Flags(Acc.A |= GBI());}
void Opc_8B_ADDA_I(void){Acc.A = Add_Byte_Flags(Acc.A,GBI());}
void Opc_8C_CMPX_I(void){Sub_Word_Flags(Reg.X,GWI());}

void Opc_8D_BSR(void)
{
   char Rel;
   Rel = (char)GBI();
   RAM[0][--Reg.S] = PC & 255;
   RAM[0][--Reg.S] = PC >>  8;
   PC += Rel;
}

void Opc_8E_LDX_I(void) {Set_Word_Flags(Reg.X = GWI());}

// ****************
// Direct A,D,X,JSR
// ****************

void Opc_90_SUBA_D(void){Acc.A = Sub_Byte_Flags(Acc.A,GBD());}
void Opc_91_CMPA_D(void){Sub_Byte_Flags(Acc.A,GBD());}
void Opc_92_SBCA_D(void){Acc.A = Sbc_Byte_Flags(Acc.A,GBD());}
void Opc_93_SUBD_D(void){Acc.D = Sub_Word_Flags(Acc.D,GWD());}
void Opc_94_ANDA_D(void){Set_Byte_Flags(Acc.A &= GBD());}
void Opc_95_BITA_D(void){Set_Byte_Flags(Acc.A &  GBD());}
void Opc_96_LDA_D(void) {Set_Byte_Flags(Acc.A = GBD());}
void Opc_97_STA_D(void) {GBD();Put_Byte_Flags(Acc.A,ea);}
void Opc_98_EORA_D(void){Set_Byte_Flags(Acc.A ^= GBD());}
void Opc_99_ADCA_D(void){Acc.A = Adc_Byte_Flags(Acc.A,GBD());}
void Opc_9A_ORA_D(void) {Set_Byte_Flags(Acc.A |= GBD());}
void Opc_9B_ADDA_D(void){Acc.A = Add_Byte_Flags(Acc.A,GBD());}
void Opc_9C_CMPX_D(void){Sub_Word_Flags(Reg.X,GWD());}

void Opc_9D_JSR_D(void)
{
   WORD A = GWD();
   RAM[0][--Reg.S] = PC & 255;
   RAM[0][--Reg.S] = PC >>  8;
   PC = A;
}

void Opc_9E_LDX_D(void) {Set_Word_Flags(Reg.X=GWD());}
void Opc_9F_STX_D(void) {Put_Word_Flags(Reg.X,(DP<<8)|GBI());}

// *****************
// Indexed A,D,X,JSR
// *****************

void Opc_A0_SUBA_P(void){Acc.A=Sub_Byte_Flags(Acc.A,GBP());}
void Opc_A1_CMPA_P(void){Sub_Byte_Flags(Acc.A,GBP());}
void Opc_A2_SBCA_P(void){Acc.A=Sbc_Byte_Flags(Acc.A,GBP());}
void Opc_A3_SUBD_P(void){Acc.D=Sub_Word_Flags(Acc.D,GWP());}
void Opc_A4_ANDA_P(void){Set_Byte_Flags(Acc.A &= GBP());}
void Opc_A5_BITA_P(void){Set_Byte_Flags(Acc.A &  GBP());}
void Opc_A6_LDA_P(void) {Set_Byte_Flags(Acc.A=GBP());}
void Opc_A7_STA_P(void) {Put_Byte_Flags(Acc.A,Post_Byte());}
void Opc_A8_EORA_P(void){Set_Byte_Flags(Acc.A ^= GBP());}
void Opc_A9_ADCA_P(void){Acc.A=Adc_Byte_Flags(Acc.A,GBP());}
void Opc_AA_ORA_P(void) {Set_Byte_Flags(Acc.A |= GBP());}
void Opc_AB_ADDA_P(void){Acc.A=Add_Byte_Flags(Acc.A,GBP());}
void Opc_AC_CMPX_P(void){Sub_Word_Flags(Reg.X,GWP());}

void Opc_AD_JSR_P(void)
{
   Post_Byte();
   RAM[0][--Reg.S] = PC & 255;
   RAM[0][--Reg.S] = PC >>  8;
   PC = ea;
}

void Opc_AE_LDX_P(void) {Set_Word_Flags(Reg.X=GWP());}
void Opc_AF_STX_P(void) {Put_Word_Flags(Reg.X,Post_Byte());}

// ******************
// Extended A,D,X,JSR
// ******************

void Opc_B0_SUBA_E(void){Acc.A=Sub_Byte_Flags(Acc.A,GBE());}
void Opc_B1_CMPA_E(void){Sub_Byte_Flags(Acc.A,GBE());}
void Opc_B2_SBCA_E(void){Acc.A=Sbc_Byte_Flags(Acc.A,GBE());}
void Opc_B3_SUBD_E(void){Acc.D=Sub_Word_Flags(Acc.D,GWE());}
void Opc_B4_ANDA_E(void){Set_Byte_Flags(Acc.A &= GBE());}
void Opc_B5_BITA_E(void){Set_Byte_Flags(Acc.A &  GBE());}
void Opc_B6_LDA_E(void) {Set_Byte_Flags(Acc.A=GBE());}
void Opc_B7_STA_E(void) {Put_Byte_Flags(Acc.A,GWI());}
void Opc_B8_EORA_E(void){Set_Byte_Flags(Acc.A ^= GBE());}
void Opc_B9_ADCA_E(void){Acc.A=Adc_Byte_Flags(Acc.A,GBE());}
void Opc_BA_ORA_E(void) {Set_Byte_Flags(Acc.A |= GBE());}
void Opc_BB_ADDA_E(void){Acc.A=Add_Byte_Flags(Acc.A,GBE());}
void Opc_BC_CMPX_E(void){Sub_Word_Flags(Reg.X,GWE());}

void Opc_BD_JSR_E(void)
{
   WORD W = GWI();
   RAM[0][--Reg.S] = PC & 255;
   RAM[0][--Reg.S] = PC >>  8;
   PC = W;
}

void Opc_BE_LDX_E(void) {Set_Word_Flags(Reg.X=GWE());}
void Opc_BF_STX_E(void) {Put_Word_Flags(Reg.X,GWI());}

// *********
// Immediate
// *********

void Opc_C0_SUBB_I(void){Acc.B=Sub_Byte_Flags(Acc.B,GBI());}
void Opc_C1_CMPB_I(void){Sub_Byte_Flags(Acc.B,GBI());}
void Opc_C2_SBCB_I(void){Acc.B=Sbc_Byte_Flags(Acc.B,GBI());}
void Opc_C3_ADDD_I(void){Acc.D=Add_Word_Flags(Acc.D,GWI());}
void Opc_C4_ANDB_I(void){Set_Byte_Flags(Acc.B &= GBI());}
void Opc_C5_BITB_I(void){Set_Byte_Flags(Acc.B &  GBI());}
void Opc_C6_LDB_I(void) {Set_Byte_Flags(Acc.B =GBI());}
void Opc_C8_EORB_I(void){Set_Byte_Flags(Acc.B ^= GBI());}
void Opc_C9_ADCB_I(void){Acc.B=Adc_Byte_Flags(Acc.B,GBI());}
void Opc_CA_ORB_I(void) {Set_Byte_Flags(Acc.B |= GBI());}
void Opc_CB_ADDB_I(void){Acc.B=Add_Byte_Flags(Acc.B,GBI());}
void Opc_CC_LDD_I(void) {Set_Word_Flags(Acc.D=GWI());}
void Opc_CD_LDQ_I(void)
{
   Acc.D = GWI();
   Acc.W = GWI();
   Set_Word_Flags(Acc.D); // emulate bug
}
void Opc_CE_LDU_I(void) {Set_Word_Flags(Reg.U=GWI());}

// ******************
// Intercept routines
// ******************

void Scroll_Up(void)
{
   SDL_Rect sr;
   SDL_Rect dr;

   Cursor_Off();
   dr.x  = 0;
   dr.w  = WinWidth;
   dr.y  = yscale * 8 * (yborder + Acc.E);
   dr.h  = yscale * 8 * (Acc.F - Acc.E);
   sr    = dr;
   sr.y += yscale * 8;
   if (*B_Jump) sr.y += (*B_Jump-1) * yscale * 8;
#ifdef SDL3
   SDL_BlitSurfaceUnchecked(EmuSur,&sr,EmuSur,&dr);
#else
   SDL_LowerBlit(EmuSur,&sr,EmuSur,&dr);
#endif
   SDL_RenderPresent(EmuRen);
}

// E = top row, F = bottom row

void Scroll_Down(void)
{
   BYTE Row;
   SDL_Rect sr;
   SDL_Rect dr;

   Cursor_Off();
   SDL_RenderPresent(EmuRen);
   dr.x  = 0;
   dr.w  = WinWidth;
   dr.y  = yscale * 8 * (yborder + Acc.F);
   dr.h  = yscale * 8;
   sr    = dr;
   for (Row = Acc.F-1 ; Row >= Acc.E ; --Row)
   {
      sr.y -= yscale * 8;
#ifdef SDL3
      SDL_BlitSurfaceUnchecked(EmuSur,&sr,EmuSur,&dr);
#else
      SDL_LowerBlit(EmuSur,&sr,EmuSur,&dr);
#endif
      dr.y = sr.y;
   }
   SDL_RenderPresent(EmuRen);
}

// ***************************************************
// * Reads 512 byte block from SD card               *
// ***************************************************
// * Input : X = start of a 512 byte sector buffer   *
// *         BIOS_LBA = 32 bit logical block address *
// * Output: B = error code (0 = OK)                 *
// *         Z flag set if success                   *
// ***************************************************

void Read_SD_Block(void)
{
   if (SD)
   {
      QUAD LBA =  (*LBA_Ptr    << 24) | (*(LBA_Ptr+1) << 16)
               | (*(LBA_Ptr+2) <<  8) |  *(LBA_Ptr+3) ;
      BYTE *Src = SD + LBA * 512;
      BYTE *Dst = &RAM[0][Reg.X];
      memcpy(Dst,Src,512);
      Acc.B = 0;
      CC |= ZERO;
   }
   else
   {
      Acc.B = 1;
      CC &= ~ZERO;
   }
}

// ***************************************************
// * Writes 512 byte block to SD card                *
// ***************************************************
// * Input : X = start of a 512 byte sector buffer   *
// *         BIOS_LBA = 32 bit logical block address *
// * Output: B = error code (0 = OK)                 *
// *         Z flag set if success                   *
// ***************************************************

void Write_SD_Block(void)
{
   if (SD)
   {
      QUAD LBA =  (*LBA_Ptr    << 24) | (*(LBA_Ptr+1) << 16)
               | (*(LBA_Ptr+2) <<  8) |  *(LBA_Ptr+3) ;
      BYTE *Dst = SD + LBA * 512;
      BYTE *Src = &RAM[0][Reg.X];
      memcpy(Dst,Src,512);
 fprintf(lf,"Buffer %4.4x -> LBA %4.4x\n",Reg.X,LBA);
      Reg.X += 512;
      Acc.B = 0;
      CC |= ZERO;
   }
   else
   {
      Acc.B = 1;
      CC &= ~ZERO;
   }
}

/*
int    tm_sec   seconds [0,61]
int    tm_min   minutes [0,59]
int    tm_hour  hour [0,23]
int    tm_mday  day of month [1,31]
int    tm_mon   month of year [0,11]
int    tm_year  years since 1900
int    tm_wday  day of week [0,6] (Sunday = 0)
int    tm_yday  day of year [0,365]
int    tm_isdst daylight savings flag
*/

void RTC_Read(void)
{
   BYTE *rtc = &RAM[0][Reg.Y];
   time_t rawtime;
   struct tm ti;
   time(&rawtime);
   ti = *localtime(&rawtime);
   rtc[0] = ti.tm_year - 100;
   rtc[1] = ti.tm_mon;
   rtc[2] = ti.tm_mday;
   rtc[3] = ti.tm_hour;
   rtc[4] = ti.tm_min;
   rtc[5] = ti.tm_sec;
}

void Opc_CF_Trap(void)
{
   WORD PM = PC-1;
        if (PM == Trap[0]) Bank = 7;
   else if (PM == Trap[1]) Bank = 0;
   else if (PM == Trap[2]) Bank = Acc.F;
   else if (PM == Trap[3]) Clear_Screen();
   else if (PM == Trap[4]) VDP_Sync_Char();
   else if (PM == Trap[5]) // Far_LDA
   {
      Acc.A = RAM[Acc.F&7][Reg.Y++];
      Set_Byte_Flags(Acc.A);
      Bank = 0;
   }
   else if (PM == Trap[6]) // Far_LDD
   {
      Acc.A = RAM[Acc.F&7][Reg.Y++];
      Acc.B = RAM[Acc.F&7][Reg.Y++];
      Set_Word_Flags(Acc.D);
      Bank = 0;
   }
   else if (PM == Trap[7]) // Far_STA
   {
      RAM[Acc.F&7][Reg.Y++] = Acc.A;
      Set_Byte_Flags(Acc.A);
      Bank = 0;
   }
   else if (PM == Trap[8]) // Far_STD
   {
      RAM[Acc.F&7][Reg.Y++] = Acc.A;
      RAM[Acc.F&7][Reg.Y++] = Acc.B;
      Set_Word_Flags(Acc.D);
      Bank = 0;
   }
   else if (PM == Trap[9]) Scroll_Up();
   else if (PM == Trap[10]) Cursor_On();
   else if (PM == Trap[11]) Cursor_Off();
   else if (PM == Trap[12]) Scroll_Down();
   else if (PM == Trap[13]) Read_SD_Block();
   else if (PM == Trap[14]) RTC_Read();
   else if (PM == Trap[15]) Write_SD_Block();
   PC = Get_Word(Reg.S);
   Reg.S  += 2;
}

// ******
// Direct
// ******

void Opc_D0_SUBB_D(void){Acc.B = Sub_Byte_Flags(Acc.B,GBD());}
void Opc_D1_CMPB_D(void){Sub_Byte_Flags(Acc.B,GBD());}
void Opc_D2_SBCB_D(void){Acc.B=Sbc_Byte_Flags(Acc.B,GBD());}
void Opc_D3_ADDD_D(void){Acc.D = Add_Word_Flags(Acc.D,GWD());}
void Opc_D4_ANDB_D(void){Set_Byte_Flags(Acc.B &= GBD());}
void Opc_D5_BITB_D(void){Set_Byte_Flags(Acc.B &  GBD());}
void Opc_D6_LDB_D(void) {Set_Byte_Flags(Acc.B = GBD());}
void Opc_D7_STB_D(void) {GBD();Put_Byte_Flags(Acc.B,ea);}
void Opc_D8_EORB_D(void){Set_Byte_Flags(Acc.B ^= GBD());}
void Opc_D9_ADCB_D(void){Acc.B=Adc_Byte_Flags(Acc.B,GBD());}
void Opc_DA_ORB_D(void) {Set_Byte_Flags(Acc.B |= GBD());}
void Opc_DB_ADDB_D(void){Acc.B = Add_Byte_Flags(Acc.B,GBD());}
void Opc_DC_LDD_D(void) {Set_Word_Flags(Acc.D = GWD());}
void Opc_DD_STD_D(void) {GBD();Put_Word_Flags(Acc.D,ea);}
void Opc_DE_LDU_D(void) {Set_Word_Flags(Reg.U = GWD());}
void Opc_DF_STU_D(void) {GBD();Put_Word_Flags(Reg.U,ea);}

// *******
// Indexed
// *******

void Opc_E0_SUBB_P(void){Acc.B=Sub_Byte_Flags(Acc.B,GBP());}
void Opc_E1_CMPB_P(void){Sub_Byte_Flags(Acc.B,GBP());}
void Opc_E2_SBCB_P(void){Acc.B=Sbc_Byte_Flags(Acc.B,GBP());}
void Opc_E3_ADDD_P(void){Acc.D=Add_Word_Flags(Acc.D,GWP());}
void Opc_E4_ANDB_P(void){Set_Byte_Flags(Acc.B &= GBP());}
void Opc_E5_BITB_P(void){Set_Byte_Flags(Acc.B &  GBP());}
void Opc_E6_LDB_P(void) {Set_Byte_Flags(Acc.B = GBP());}
void Opc_E7_STB_P(void) {Put_Byte_Flags(Acc.B,Post_Byte());}
void Opc_E8_EORB_P(void){Set_Byte_Flags(Acc.B ^= GBP());}
void Opc_E9_ADCB_P(void){Acc.B=Adc_Byte_Flags(Acc.B,GBP());}
void Opc_EA_ORB_P(void) {Set_Byte_Flags(Acc.B |= GBP());}
void Opc_EB_ADDB_P(void){Acc.B=Add_Byte_Flags(Acc.B,GBP());}
void Opc_EC_LDD_P(void) {Set_Word_Flags(Acc.D = GWP());}
void Opc_ED_STD_P(void) {Put_Word_Flags(Acc.D,Post_Byte());}
void Opc_EE_LDU_P(void) {Set_Word_Flags(Reg.U = GWP());}
void Opc_EF_STU_P(void) {Put_Word_Flags(Reg.U,Post_Byte());}

// ********
// Extended
// ********

void Opc_F0_SUBB_E(void){Acc.B=Sub_Byte_Flags(Acc.B,GBE());}
void Opc_F1_CMPB_E(void){Sub_Byte_Flags(Acc.B,GBE());}
void Opc_F2_SBCB_E(void){Acc.B=Sbc_Byte_Flags(Acc.B,GBE());}
void Opc_F3_ADDD_E(void){Acc.D=Add_Word_Flags(Acc.D,GWE());}
void Opc_F4_ANDB_E(void){Set_Byte_Flags(Acc.B &= GBE());}
void Opc_F5_BITB_E(void){Set_Byte_Flags(Acc.B &  GBE());}
void Opc_F6_LDB_E(void) {Set_Byte_Flags(Acc.B = GBE());}
void Opc_F7_STB_E(void) {Put_Byte_Flags(Acc.B,GWI());}
void Opc_F8_EORB_E(void){Set_Byte_Flags(Acc.B ^= GBE());}
void Opc_F9_ADCB_E(void){Acc.B=Adc_Byte_Flags(Acc.B,GBE());}
void Opc_FA_ORB_E(void) {Set_Byte_Flags(Acc.B |= GBE());}
void Opc_FB_ADDB_E(void){Acc.B=Add_Byte_Flags(Acc.B,GBE());}
void Opc_FC_LDD_E(void) {Set_Word_Flags(Acc.D = GWE());}
void Opc_FD_STD_E(void) {Put_Word_Flags(Acc.D,GWI());}
void Opc_FE_LDU_E(void) {Set_Word_Flags(Reg.U = GWE());}
void Opc_FF_STU_E(void) {Put_Word_Flags(Reg.U,GWI());}


// ***********
// Long branch
// ***********

void LB_Cond(BYTE cond)
{
   signed short Rel = GWI();
   if (cond) PC += Rel;
}

void O10_21_LBRN(void) {PC += 2;}
void O10_22_LBHI(void) {LB_Cond(!(CC & (ZERO | CARRY)));}
void O10_23_LBLS(void) {LB_Cond(  CC & (ZERO | CARRY) );}
void O10_24_LBCC(void) {LB_Cond(!(CC & CARRY   ));}
void O10_25_LBCS(void) {LB_Cond(  CC & CARRY    );}
void O10_26_LBNE(void) {LB_Cond(!(CC & ZERO    ));}
void O10_27_LBEQ(void) {LB_Cond(  CC & ZERO     );}
void O10_28_LBVC(void) {LB_Cond(!(CC & OVERFL  ));}
void O10_29_LBVS(void) {LB_Cond(  CC & OVERFL   );}
void O10_2A_LBPL(void) {LB_Cond(!(CC & NEGATIVE));}
void O10_2B_LBMI(void) {LB_Cond(  CC & NEGATIVE );}
void O10_2C_LBGE(void) {LB_Cond(  (CC&10)==10 || (CC&10)==0) ;}
void O10_2D_LBLT(void) {LB_Cond(  (CC&10)== 8 || (CC&10)==2) ;}
void O10_2E_LBGT(void) {LB_Cond(  (CC&14)==10 || (CC&14)==0) ;}
void O10_2F_LBLE(void) {LB_Cond(!((CC&14)==10 || (CC&14)==0));}

// *******************
// Register - Register
// *******************

void O10_30_ADDR(void)
{
   BYTE rs,rd;
   WORD vs,vd;
   pb = GBI();
   rs = pb >> 4;
   rd = pb & 15;
   vs = Get_Reg_Val(rs);
   vd = Get_Reg_Val(rd);
   if (rd < 8) vd = Add_Word_Flags(vd,vs);
   else        vd = Add_Byte_Flags(vd,vs);
   Put_Reg_Val(vd,rd);
}

void O10_31_ADCR(void)
{
   BYTE rs,rd;
   WORD vs,vd;
   pb = GBI();
   rs = pb >> 4;
   rd = pb & 15;
   vs = Get_Reg_Val(rs);
   vd = Get_Reg_Val(rd);
   if (rd < 8) vd = Adc_Word_Flags(vd,vs);
   else        vd = Adc_Byte_Flags(vd,vs);
   Put_Reg_Val(vd,rd);
}

void O10_32_SUBR(void)
{
   BYTE rs,rd;
   WORD vs,vd;
   pb = GBI();
   rs = pb >> 4;
   rd = pb & 15;
   vs = Get_Reg_Val(rs);
   vd = Get_Reg_Val(rd);
   if (rd < 8) vd = Sub_Word_Flags(vd,vs);
   else        vd = Sub_Byte_Flags(vd,vs);
   Put_Reg_Val(vd,rd);
}

void O10_33_SBCR(void)
{
   BYTE rs,rd;
   WORD vs,vd;
   pb = GBI();
   rs = pb >> 4;
   rd = pb & 15;
   vs = Get_Reg_Val(rs);
   vd = Get_Reg_Val(rd);
   if (rd < 8) vd = Sbc_Word_Flags(vd,vs);
   else        vd = Sbc_Byte_Flags(vd,vs);
   Put_Reg_Val(vd,rd);
}

void O10_34_ANDR(void)
{
   BYTE rs,rd;
   WORD vd;
   pb = GBI();
   rs = pb >> 4;
   rd = pb & 15;
   vd = Get_Reg_Val(rs) & Get_Reg_Val(rd);
   if (rd < 8) Set_Word_Flags(vd);
   else        Set_Byte_Flags(vd);
   Put_Reg_Val(vd,rd);
}

void O10_35_ORR(void)
{
   BYTE rs,rd;
   WORD vd;
   pb = GBI();
   rs = pb >> 4;
   rd = pb & 15;
   vd = Get_Reg_Val(rs) | Get_Reg_Val(rd);
   if (rd < 8) Set_Word_Flags(vd);
   else        Set_Byte_Flags(vd);
   Put_Reg_Val(vd,rd);
}

void O10_36_EORR(void)
{
   BYTE rs,rd;
   WORD vd;
   pb = GBI();
   rs = pb >> 4;
   rd = pb & 15;
   vd = Get_Reg_Val(rs) ^ Get_Reg_Val(rd);
   if (rd < 8) Set_Word_Flags(vd);
   else        Set_Byte_Flags(vd);
   Put_Reg_Val(vd,rd);
}

void O10_37_CMPR(void)
{
   BYTE rs,rd;
   WORD vs,vd;
   pb = GBI();
   rs = pb >> 4;
   rd = pb & 15;
   vs = Get_Reg_Val(rs);
   vd = Get_Reg_Val(rd);
   if (rd < 8) Sub_Word_Flags(vd,vs);
   else        Sub_Byte_Flags(vd,vs);
}

void O10_38_PSHSW(void) {Put_Word(Acc.W,Reg.S-=2); }
void O10_39_PULSW(void) {Acc.W = Get_Word(Reg.S); Reg.S+=2; }
void O10_3A_PSHUW(void) {Put_Word(Acc.W,Reg.U-=2); }
void O10_3B_PULUW(void) {Acc.W = Get_Word(Reg.U); Reg.U+=2; }
void O10_3F_SWI2(void)  {Push_All(); PC = Get_Word(0xfff4); }

// **************
//  D Accumulator
// **************

void O10_40_NEGD(void)
{
   if (Acc.D == 0x8000) CC |= OVERFL;
   else Set_Word_Flags(Acc.D = -Acc.D);
}

void O10_43_COMD(void)  {Set_Word_Flags(Acc.D=~Acc.D);CC |= CARRY;}

void O10_44_LSRD(void)
{
   if (Acc.D & 1) CC |=  CARRY;
   else           CC &= ~CARRY;
   Set_Word_Flags(Acc.D >>= 1);
}

void O10_46_RORD(void)
{
   WORD C = Acc.D & 1;
   Acc.D >>= 1;
   if (CC & CARRY) Acc.D |= 0x8000;
   Set_Word_Flags(Acc.D);
   if (C) CC |=  CARRY;
   else   CC &= ~CARRY;
}

void O10_47_ASRD(void)
{
   WORD C = Acc.D & 1;
   Acc.D >>= 1;
   if (Acc.D & 0x4000) Acc.D |= 0x8000;
   Set_Word_Flags(Acc.D);
   if (C) CC |=  CARRY;
   else   CC &= ~CARRY;
}

void O10_48_ASLD(void)
{
   WORD C = Acc.D & 0x8000;
   Acc.D <<= 1;
   Set_Word_Flags(Acc.D);
   if (C) CC |=  CARRY;
   else   CC &= ~CARRY;
}

void O10_49_ROLD(void)
{
   WORD C = Acc.D & 0x8000;
   Acc.D <<= 1;
   if (CC & CARRY) Acc.D |= 1;
   Set_Word_Flags(Acc.D);
   if (C) CC |=  CARRY;
   else   CC &= ~CARRY;
}

void O10_4A_DECD(void)  {Set_Word_Flags(--Acc.D);if(Acc.D==0x7fff)CC=OVERFL;}
void O10_4C_INCD(void)  {Set_Word_Flags(++Acc.D);if(Acc.D==0x8000)CC=OVERFL;}
void O10_4D_TSTD(void)  {Set_Word_Flags(Acc.D); }
void O10_4F_CLRD(void)  {Acc.D = 0; CC = (CC & 0xf0) | ZERO;}

void O10_53_COMW(void)  {Set_Word_Flags(Acc.W=~Acc.W);CC |= CARRY;}

void O10_54_LSRW(void)
{
   if (Acc.W & 1) CC |=  CARRY;
   else           CC &= ~CARRY;
   Set_Word_Flags(Acc.W >>= 1);
}

void O10_56_RORW(void)
{
   WORD C = Acc.W & 1;
   Acc.W >>= 1;
   if (CC & CARRY) Acc.W |= 0x8000;
   Set_Word_Flags(Acc.W);
   if (C) CC |=  CARRY;
   else   CC &= ~CARRY;
}

void O10_59_ROLW(void)
{
   WORD C = Acc.W & 0x8000;
   Acc.W <<= 1;
   if (CC & CARRY) Acc.W |= 1;
   Set_Word_Flags(Acc.W);
   if (C) CC |=  CARRY;
   else   CC &= ~CARRY;
}

void O10_5A_DECW(void)  {Set_Word_Flags(--Acc.W);if(Acc.W==0x7fff)CC=OVERFL;}
void O10_5C_INCW(void)  {Set_Word_Flags(++Acc.W);if(Acc.W==0x8000)CC=OVERFL;}
void O10_5D_TSTW(void)  {Set_Word_Flags(Acc.W);}
void O10_5F_CLRW(void)  {Acc.W = 0; CC = (CC& 0xf0) | ZERO;}

void O10_80_SUBW_I(void){Acc.W = Sub_Word_Flags(Acc.W,GWI());}
void O10_81_CMPW_I(void){Sub_Word_Flags(Acc.W,GWI());}
void O10_82_SBCD_I(void){Acc.D=Sbc_Word_Flags(Acc.D,GWI());}
void O10_83_CMPD_I(void){Sub_Word_Flags(Acc.D , GWI());}
void O10_84_ANDD_I(void){Set_Word_Flags(Acc.D&= GWI());}
void O10_85_BITD_I(void){Set_Word_Flags(Acc.D & GWI());}
void O10_86_LDW_I(void) {Set_Word_Flags(Acc.W = GWI());}
void O10_88_EORD_I(void){Set_Word_Flags(Acc.D^= GWI());}
void O10_89_ADCD_I(void){Acc.D = Adc_Word_Flags(Acc.D,GWI());}
void O10_8A_ORD_I(void) {Set_Word_Flags(Acc.D|= GWI());}
void O10_8B_ADDW_I(void){Acc.W = Add_Word_Flags(Acc.W,GWI());}
void O10_8C_CMPY_I(void){Sub_Word_Flags(Reg.Y,GWI());}
void O10_8E_LDY_I(void) {Set_Word_Flags(Reg.Y = GWI());}

void O10_90_SUBW_D(void){Acc.W = Sub_Word_Flags(Acc.W,GWD());}
void O10_91_CMPW_D(void){Sub_Word_Flags(Acc.W,GWD());}
void O10_92_SBCD_D(void){Acc.D=Sbc_Word_Flags(Acc.D,GWD());}
void O10_93_CMPD_D(void){Sub_Word_Flags(Acc.D,GWD());}
void O10_94_ANDD_D(void){Set_Word_Flags(Acc.D&= GWD());}
void O10_95_BITD_D(void){Set_Word_Flags(Acc.D & GWD());}
void O10_96_LDW_D(void) {Set_Word_Flags(Acc.W = GWD());}
void O10_97_STW_D(void) {Put_Word_Flags(Acc.W,(DP<<8)|GBI());}
void O10_98_EORD_D(void){Set_Word_Flags(Acc.D^= GWD());}
void O10_99_ADCD_D(void){Acc.D = Adc_Word_Flags(Acc.D,GWD());}
void O10_9A_ORD_D(void) {Set_Word_Flags(Acc.D|= GWD());}
void O10_9B_ADDW_D(void){Acc.W = Add_Word_Flags(Acc.W,GWD());}
void O10_9C_CMPY_D(void){Sub_Word_Flags(Reg.Y,GWD());}
void O10_9E_LDY_D(void) {Set_Word_Flags(Reg.Y = GWD());}
void O10_9F_STY_D(void) {Put_Word_Flags(Reg.Y,(DP<<8)|GBI());}

void O10_A0_SUBW_P(void){Acc.W = Sub_Word_Flags(Acc.W,GWP());}
void O10_A1_CMPW_P(void){Sub_Word_Flags(Acc.W,GWP());}
void O10_A2_SBCD_P(void){Acc.D=Sbc_Word_Flags(Acc.D,GWP());}
void O10_A3_CMPD_P(void){Sub_Word_Flags(Acc.D,GWP());}
void O10_A4_ANDD_P(void){Set_Word_Flags(Acc.D&= GWP());}
void O10_A5_BITD_P(void){Set_Word_Flags(Acc.D & GWP());}
void O10_A6_LDW_P(void) {Set_Word_Flags(Acc.W = GWP());}
void O10_A7_STW_P(void) {Put_Word_Flags(Acc.W,Post_Byte());}
void O10_A8_EORD_P(void){Set_Word_Flags(Acc.D^= GWP());}
void O10_A9_ADCD_P(void){Acc.D = Adc_Word_Flags(Acc.D,GWP());}
void O10_AA_ORD_P(void) {Set_Word_Flags(Acc.D|= GWP());}
void O10_AB_ADDW_P(void){Acc.W = Add_Word_Flags(Acc.W,GWP());}
void O10_AC_CMPY_P(void){Sub_Word_Flags(Reg.Y,GWP());}
void O10_AE_LDY_P(void) {Set_Word_Flags(Reg.Y = GWP());}
void O10_AF_STY_P(void) {Put_Word_Flags(Reg.Y,Post_Byte());}

void O10_B0_SUBW_E(void){Acc.W=Sub_Word_Flags(Acc.W,GWE());}
void O10_B1_CMPW_E(void){Sub_Word_Flags(Acc.W,GWE());}
void O10_B2_SBCD_E(void){Acc.D=Sbc_Word_Flags(Acc.D,GWE());}
void O10_B3_CMPD_E(void){Sub_Word_Flags(Acc.D,GWE());}
void O10_B4_ANDD_E(void){Set_Word_Flags(Acc.D&= GWE());}
void O10_B5_BITD_E(void){Set_Word_Flags(Acc.D & GWE());}
void O10_B6_LDW_E(void) {Set_Word_Flags(Acc.W = GWE());}
void O10_B7_STW_E(void) {Put_Word_Flags(Acc.W,GWI());}
void O10_B8_EORD_E(void){Set_Word_Flags(Acc.D^= GWE());}
void O10_B9_ADCD_E(void){Acc.D=Adc_Word_Flags(Acc.D,GWE());}
void O10_BA_ORD_E(void) {Set_Word_Flags(Acc.D|= GWE());}
void O10_BB_ADDW_E(void){Acc.W=Add_Word_Flags(Acc.W,GWE());}
void O10_BC_CMPY_E(void){Sub_Word_Flags(Reg.Y,GWE());}
void O10_BE_LDY_E(void) {Set_Word_Flags(Reg.Y = GWE());}
void O10_BF_STY_E(void) {Put_Word_Flags(Reg.Y,GWI());}

void O10_CE_LDS_I(void) {Set_Word_Flags(Reg.S = GWI());}

void O10_DC_LDQ_D(void)
{
   WORD Adr = (DP << 8) | GBI();
   Acc.D = Get_Word(Adr);
   Acc.W = Get_Word(Adr+2);
   Set_Word_Flags(Acc.D); // CPU bug - should be Q
}

void O10_DD_STQ_D(void)
{
   WORD Adr = (DP << 8) | GBI();
   Put_Quad(Acc.Q,Adr);
   Set_Word_Flags(Acc.D); // CPU bug - should be Q
}

void O10_DE_LDS_D(void) {Set_Word_Flags(Reg.S = GWD());}
void O10_DF_STS_D(void) {Put_Word_Flags(Reg.S,(DP<<8)|GBI());}

void O10_EC_LDQ_P(void)
{
   Post_Byte();
   Acc.D = Get_Word(ea);
   Acc.W = Get_Word(ea+2);
   Set_Quad_Flags(Acc.Q);
}

void O10_ED_STQ_P(void)
{
   Post_Byte();
   Put_Word(Acc.D,ea);
   Put_Word(Acc.W,ea+2);
   Set_Quad_Flags(Acc.Q);
}

void O10_EE_LDS_P(void) {Set_Word_Flags(Reg.S = GWP());}
void O10_EF_STS_P(void) {Put_Word_Flags(Reg.S,Post_Byte());}

void O10_FC_LDQ_E(void)
{
   WORD Adr = GWI();
   Acc.D = Get_Word(Adr);
   Acc.W = Get_Word(Adr+2);
   Set_Word_Flags(Acc.D); // CPU bug - should be Q
}

void O10_FD_STQ_E(void)
{
   WORD Adr = GWI();
   Put_Quad(Acc.Q,Adr);
   Set_Word_Flags(Acc.D); // CPU bug - should be Q
}

void O10_FE_LDS_E(void) {Set_Word_Flags(Reg.S = GWE());}
void O10_FF_STS_E(void) {Put_Word_Flags(Reg.S,GWI());}

void O11_3X_BITOP(void)
{
   BYTE *Reg;
   BYTE P = GBI();             // RR-SSS-DDD
   WORD A = (DP << 8) | GBI(); // DP address
   BYTE srcbit = (P >> 3) & 7;
   BYTE dstbit =  P  & 7;
   BYTE regnum =  P >> 6;
   BYTE srcval;
        if (regnum == 0) Reg = &CC;
   else if (regnum == 1) Reg = &Acc.A;
   else if (regnum == 2) Reg = &Acc.B;
   else
   {
      fprintf(lf,"Invalid BAND postbyte %2.2x at PC %4.4x\n",P,PC-4);
      return;
   }
   switch (oc)
   {
      case 0x30: // BAND
         if (!( Get_Byte(A) & Valbit[srcbit])) *Reg &= ~Valbit[dstbit];break;
      case 0x31: // BIAND
         if (!(~Get_Byte(A) & Valbit[srcbit])) *Reg &= ~Valbit[dstbit];break;
      case 0x32: // BOR
         if ( ( Get_Byte(A) & Valbit[srcbit])) *Reg |=  Valbit[dstbit];break;
      case 0x33: // BIOR
         if ( (~Get_Byte(A) & Valbit[srcbit])) *Reg |=  Valbit[dstbit];break;
      case 0x34: // BEOR
         if ( ( Get_Byte(A) & Valbit[srcbit])) *Reg ^=  Valbit[dstbit];break;
      case 0x35: // BIEOR
         if ( (~Get_Byte(A) & Valbit[srcbit])) *Reg ^=  Valbit[dstbit];break;
      case 0x36: // LDBT
         *Reg &= ~Valbit[dstbit];
         if (Get_Byte(A) & Valbit[srcbit]) *Reg |= Valbit[dstbit];     break;
      case 0x37: // STBT
         srcval = Get_Byte(A) & ~Valbit[srcbit];
         if (*Reg & Valbit[dstbit]) srcval |= Valbit[srcbit];
         Put_Byte(srcval,A);
   }
}


WORD *TFM_Reg(BYTE r)
{
   switch (r)
   {
      case  1: return &Reg.X;
      case  2: return &Reg.Y;
      case  3: return &Reg.U;
      case  4: return &Reg.S;
      default: return &Acc.D;
   }
}

void O11_3X_TFM(void)
{
   WORD *Source;
   WORD *Target;
   BYTE Reg,Val;
   Reg = GBI();
   Source = TFM_Reg(Reg >> 4);
   Target = TFM_Reg(Reg & 15);
   if (oc == 0x38)
   {
      while (Acc.W)
      {
         Put_Byte(Get_Byte((*Source)++),(*Target)++);
         --Acc.W;
      }
   }
   else if (oc == 0x39)
   {
      while (Acc.W)
      {
         Put_Byte(Get_Byte((*Source)--),(*Target)--);
         --Acc.W;
      }

   }
   else if (oc == 0x3a)
   {
      while (Acc.W)
      {
         Put_Byte(Get_Byte((*Source)++),(*Target));
         --Acc.W;
      }
   }
   else // 0x3b
   {
      Val = Get_Byte(*Source);
      while (Acc.W)
      {
         Put_Byte(Val,(*Target)++);
         --Acc.W;
      }
   }
}

void O11_3C_BITMD(void)
{
   BYTE V = GBI() & 0xc0; // Only bits 7 and 6
   if (V & MD) CC &= ~ZERO;        // either or both were set
   else        CC |=  ZERO;        // Tested bits were clear
   MD &= ~V;                       // Clear tested bits
}
void O11_3D_LDMD(void)  {MD = GBI() & 3;}
void O11_3F_SWI3(void)  {Push_All(); PC = Get_Word(0xfff2);}

// **********
// E register
// **********

void O11_43_COME(void)  {Set_Byte_Flags(Acc.E=~Acc.E); CC |= CARRY;}
void O11_4A_DECE(void)  {Set_Byte_Flags(--Acc.E);if (Acc.E==0x7f) CC|=OVERFL;}
void O11_4C_INCE(void)  {Set_Byte_Flags(++Acc.E);if (Acc.E==0x80) CC|=OVERFL;}
void O11_4D_TSTE(void)  {Set_Byte_Flags(Acc.E);}
void O11_4F_CLRE(void)  {Acc.E = 0; CC = (CC & 0xf0) | ZERO;}

// **********
// F register
// **********

void O11_53_COMF(void)  {Set_Byte_Flags(Acc.F=~Acc.F); CC |= CARRY;}
void O11_5A_DECF(void)  {Set_Byte_Flags(--Acc.F);if (Acc.F==0x7f) CC|=OVERFL;}
void O11_5C_INCF(void)  {Set_Byte_Flags(++Acc.F);if (Acc.F==0x80) CC|=OVERFL;}
void O11_5D_TSTF(void)  {Set_Byte_Flags(Acc.F); }
void O11_5F_CLRF(void)  {Acc.F = 0; CC = (CC & 0xf0) | ZERO;}

// *********
// Immediate
// *********

void O11_80_SUBE_I(void){Acc.E=Sub_Byte_Flags(Acc.E,GBI());}
void O11_81_CMPE_I(void){      Sub_Byte_Flags(Acc.E,GBI());}
void O11_83_CMPU_I(void){      Sub_Word_Flags(Reg.U,GWI());}
void O11_86_LDE_I(void) {      Set_Byte_Flags(Acc.E=GBI());}
void O11_8B_ADDE_I(void){Acc.E=Add_Byte_Flags(Acc.E,GBI());}
void O11_8C_CMPS_I(void){      Sub_Word_Flags(Reg.S,GWI());}

// ********
// Division
// ********

void O11_XD_DIVD(void)
{
   char Divisor;
        if (oc == 0x8d) Divisor = (char)GBI();
   else if (oc == 0x9d) Divisor = (char)GBD();
   else if (oc == 0xad) Divisor = (char)GBP();
   else                 Divisor = (char)GBE();
   if (Divisor == 0)
   {
      MD |= 0x80; // Set division by zero flag
      Push_All();
      PC = Get_Word(0xfff0);
      return;
   }
   int Q = (signed short)Acc.D / Divisor;
   int R = (signed short)Acc.D % Divisor;
   if (Q > 255) CC = (CC & 0xf0) | OVERFL;
   else
   {
      Acc.A = R;
      Set_Byte_Flags(Acc.B = Q);
      if (Acc.B & 1) CC |=  CARRY;
      else           CC &= ~CARRY;
   }
}

void O11_XE_DIVQ(void)
{
   short Divisor;
        if (oc == 0x8e) Divisor = (short)GWI();
   else if (oc == 0x9e) Divisor = (short)GWD();
   else if (oc == 0xae) Divisor = (short)GWP();
   else                 Divisor = (short)GWE();
   if (Divisor == 0)
   {
      MD |= 0x80; // Set division by zero flag
      Push_All();
      PC = Get_Word(0xfff0);
      return;
   }
   int Q = (int)Acc.Q / Divisor;
   int R = (int)Acc.Q % Divisor;
   if (Q > 0xffff) CC = (CC & 0xf0) | OVERFL;
   else
   {
      Acc.D = R;
      Set_Byte_Flags(Acc.W = Q);
      if (Acc.W & 1) CC |=  CARRY;
      else           CC &= ~CARRY;
   }
}

void O11_XF_MULD(void)
{
   short Factor;
        if (oc == 0x8f) Factor = (short)GWI();
   else if (oc == 0x9f) Factor = (short)GWD();
   else if (oc == 0xaf) Factor = (short)GWP();
   else                 Factor = (short)GWE();
   Acc.Q = (short)Acc.D * Factor;
   Set_Quad_Flags(Acc.Q);
}

// ******
// Direct
// ******

void O11_90_SUBE_D(void){Acc.E=Sub_Byte_Flags(Acc.E,GBD());}
void O11_91_CMPE_D(void){      Sub_Byte_Flags(Acc.E,GBD());}
void O11_93_CMPU_D(void){      Sub_Word_Flags(Reg.U,GWD());}
void O11_96_LDE_D(void) {      Set_Byte_Flags(Acc.E=GBD());}
void O11_97_STE_D(void) {GBD();Put_Byte_Flags(Acc.E,ea   );}
void O11_9B_ADDE_D(void){Acc.E=Add_Byte_Flags(Acc.E,GBD());}
void O11_9C_CMPS_D(void){      Sub_Word_Flags(Reg.S,GWD());}

// *******
// Indexed
// *******

void O11_A0_SUBE_P(void){Acc.E=Sub_Byte_Flags(Acc.E,GBP());}
void O11_A1_CMPE_P(void){      Sub_Byte_Flags(Acc.E,GBP());}
void O11_A3_CMPU_P(void){      Sub_Word_Flags(Reg.U,GWP());}
void O11_A6_LDE_P(void) {      Set_Byte_Flags(Acc.E=GBP());}
void O11_A7_STE_P(void) {GBP();Put_Byte_Flags(Acc.E,ea   );}
void O11_AB_ADDE_P(void){Acc.E=Add_Byte_Flags(Acc.E,GBP());}
void O11_AC_CMPS_P(void){      Sub_Word_Flags(Reg.S,GWP());}

// ********
// Extended
// ********

void O11_B0_SUBE_E(void){Acc.E=Sub_Byte_Flags(Acc.E,GBE());}
void O11_B1_CMPE_E(void){      Sub_Byte_Flags(Acc.E,GBE());}
void O11_B3_CMPU_E(void){      Sub_Word_Flags(Reg.U,GWE());}
void O11_B6_LDE_E(void) {      Set_Byte_Flags(Acc.E=GBE());}
void O11_B7_STE_E(void) {      Put_Byte_Flags(Acc.E,GWI());}
void O11_BB_ADDE_E(void){Acc.E=Add_Byte_Flags(Acc.E,GBE());}
void O11_BC_CMPS_E(void){      Sub_Word_Flags(Reg.S,GWE());}

// *********
// Immediate
// *********

void O11_C0_SUBF_I(void){Acc.F=Sub_Byte_Flags(Acc.F,GBI());}
void O11_C1_CMPF_I(void){      Sub_Byte_Flags(Acc.F,GBI());}
void O11_C6_LDF_I(void) {      Set_Byte_Flags(Acc.F=GBI());}
void O11_CB_ADDF_I(void){Acc.F=Add_Byte_Flags(Acc.F,GBI());}

// ******
// Direct
// ******

void O11_D0_SUBF_D(void){Acc.F=Sub_Byte_Flags(Acc.F,GBD());}
void O11_D1_CMPF_D(void){      Sub_Byte_Flags(Acc.F,GBD());}
void O11_D6_LDF_D(void) {      Set_Byte_Flags(Acc.F=GBD());}
void O11_D7_STF_D(void) {GBD();Put_Byte_Flags(Acc.F,ea   );}
void O11_DB_ADDF_D(void){Acc.F=Add_Byte_Flags(Acc.F,GBD());}

// *******
// Indexed
// *******

void O11_E0_SUBF_P(void){Acc.F=Sub_Byte_Flags(Acc.F,GBP());}
void O11_E1_CMPF_P(void){      Sub_Byte_Flags(Acc.F,GBP());}
void O11_E6_LDF_P(void) {      Set_Byte_Flags(Acc.F=GBP());}
void O11_E7_STF_P(void) {GBP();Put_Byte_Flags(Acc.F,ea   );}
void O11_EB_ADDF_P(void){Acc.F=Add_Byte_Flags(Acc.F,GBP());}

// ********
// Extended
// ********

void O11_F0_SUBF_E(void){Acc.F=Sub_Byte_Flags(Acc.F,GBE());}
void O11_F1_CMPF_E(void){      Sub_Byte_Flags(Acc.F,GBE());}
void O11_F6_LDF_E(void) {      Set_Byte_Flags(Acc.F=GBE());}
void O11_F7_STF_E(void) {      Put_Byte_Flags(Acc.F,GWI());}
void O11_FB_ADDF_E(void){Acc.F=Add_Byte_Flags(Acc.F,GBE());}

// ****
// Step
// ****

void Step(void)
{
   if (PC == 0xffff)
   {
      Print_Status(lf);
      SDL_Delay(3000);
      exit(1);
   }
   pg = 0;
   oc = Prg_Byte();
   if (oc == 0x10)
   {
      pg = 0x10;
      oc = Prg_Byte();
      O10[oc]();
   }
   else if (oc == 0x11)
   {
      pg = 0x11;
      oc = Prg_Byte();
      O11[oc]();
   }
   else Opc[oc]();
}

#if GERKBD == 1        //  0   1   2   3   4   5   6   7   8   9
char Digit_Shift[10] = {'=','!','"','@','$','%','&','/','(',')'};
//                       ^  sz acc   +   #   <   ,   .   -   ,   .   /
#define SYMS_DIM 12
char SymbolShift[12] = {'?','`','U','*', 39,'Z','O','A','X',';',':','_'};
//                       45  46  47  48  49  50  51  52  53  54  55  56
#define SPEC_DIM  7
//                           `   ^   <>     sz   ae   oe   ue
BYTE Ger_Scn[SPEC_DIM] = {  46,  53, 100,   45,  52,  51,  47};
BYTE Ger_Key[SPEC_DIM] = {  39, '^', '<',0xE1,0x84,0x94,0x81};
BYTE Ger_Shi[SPEC_DIM] = {0x60,0xF8, '>', '?',0x8E,0x99,0x9A};
#define ALT_DIM  9
//                           7    8    9    0   sz    q euro    +    <
BYTE Ger_Als[ALT_DIM]  = {  36,  37,  38,  39,  45,  20,   8,  48, 100};
BYTE Ger_Alt[ALT_DIM]  = { '{', '[', ']', '}',0x5C, '@',0x9E, '~', '|'};
#else
char Digit_Shift[10] = {')','!','@','#','$','%','^','&','*','('};
//                       -   =   [   ]   \   \   ;   '   `   ,   .   /
#define SYMS_DIM 12
char SymbolShift[12] = { '_','+','{','}','|','|',':', 34,'~','<','>','?'};
//                        45  46  47  48  49  50  51  52  53  54  55  56
#define SPEC_DIM  0
#endif

//                      >  <  V  ^
char Cursor_Code[ 4] = {4,19,24, 5};

void Buffer(BYTE Key)
{
   WORD wp;
   wp = Get_Word(keywrp);
   Put_Byte(Key,wp);
   if (++wp >= KEYBUF+16) wp = KEYBUF;
   Put_Word(wp,keywrp);
}

/*
typedef struct SDL_KeyboardEvent // SDL3
{
    SDL_EventType type;     // SDL_EVENT_KEY_DOWN or SDL_EVENT_KEY_UP
    Uint32 reserved;
    Uint64 timestamp;       // In nanoseconds, populated using SDL_GetTicksNS()
    SDL_WindowID windowID;  // The window with keyboard focus, if any
    SDL_KeyboardID which;   // The keyboard instance id, or 0 if unknown or virtual
    SDL_Scancode scancode;  // SDL physical key code
    SDL_Keycode key;        // SDL virtual key code
    SDL_Keymod mod;         // current key modifiers
    Uint16 raw;             // The platform dependent scancode for this event
    bool down;              // true if the key is pressed
    bool repeat;            // true if this is a key repeat
} SDL_KeyboardEvent;

typedef struct SDL_KeyboardEvent // SDL2
{
    Uint32 type;            // SDL_KEYDOWN or SDL_KEYUP
    Uint32 timestamp;       // In milliseconds, populated using SDL_GetTicks()
    Uint32 windowID;        // The window with keyboard focus, if any
    Uint8 state;            // SDL_PRESSED or SDL_RELEASED
    Uint8 repeat;           // Non-zero if this is a key repeat
    Uint8 padding2;
    Uint8 padding3;
    SDL_Keysym keysym;      // The key that was pressed or released
} SDL_KeyboardEvent;

typedef struct SDL_Keysym   // SDL2
{
    SDL_Scancode scancode;  // SDL physical key code - see SDL_Scancode for details
    SDL_Keycode sym;        // SDL virtual key code - see SDL_Keycode for details
    Uint16 mod;             // current key modifiers
    Uint32 unused;
} SDL_Keysym;

*/

void Key_Event(SDL_Event event)
{
   int i;
   SDL_Scancode Scn;        // SDL physical key code
   SDL_Keycode  Key;        // SDL virtual key code
   SDL_Keymod   Mod;        // current key modifiers
   Uint16       Raw;        // The platform dependent scancode for this event
   BYTE      EmuKey;
#ifdef SDL3
   Scn = event.key.scancode;
   Key = event.key.key;
   Mod = event.key.mod;
   Raw = event.key.raw;
#else
   Scn = event.key.keysym.scancode;
   Key = event.key.keysym.sym;
   Mod = event.key.keysym.mod;
   Raw = Scn;
#endif
   EmuKey = (BYTE)Key;

// printf("Mod:%2d Raw:%2d Scn:%2d Key:%2d [%c]\n",Mod,Raw,Scn,EmuKey,EmuKey);

   if (EmuKey == CR) EmuKey = LF; // Sparrow line separator

// Handle shifted keys

   else if (Mod == SDL_KMOD_LSHIFT || Mod == SDL_KMOD_RSHIFT)
   {
           if (isalpha(EmuKey)) EmuKey = toupper(EmuKey);
      else if (isdigit(EmuKey)) EmuKey = Digit_Shift[EmuKey-'0'];
      else if (Scn >= 45 && Scn < 45+SYMS_DIM) EmuKey = SymbolShift[Scn-45];
   }

// Handle control codes Ctrl-A -> Ctrl-Z

   else if (Mod == SDL_KMOD_LCTRL || Mod == SDL_KMOD_RCTRL)
   {
           if (isalpha(EmuKey)) EmuKey &= 31;
   }
   if (Scn >= 79 && Scn <= 82) EmuKey = Cursor_Code[Scn-79];

// Special keys on German keyboards
#ifdef GERKBD
   for (i=0 ; i < SPEC_DIM ; ++i)
   {
      if (Scn == Ger_Scn[i])
      {
         if (Mod == SDL_KMOD_LSHIFT || Mod == SDL_KMOD_RSHIFT)
              EmuKey = Ger_Shi[i];
         else EmuKey = Ger_Key[i];
      }
   }
   if (Mod == SDL_KMOD_LALT || Mod == SDL_KMOD_RALT)
   for (i=0 ; i < ALT_DIM ; ++i)
   {
      if (Scn == Ger_Als[i])
      {
         EmuKey = Ger_Alt[i];
      }
   }
#endif
   if (Scn < 224) Buffer(EmuKey);
}

void Save_SD(void)
{
   FILE *sd;
   sd = fopen(SD_fn,"wb");
   if (sd)
   {
      fwrite(SD,1,SD_Size,sd);
      fclose(sd);
   }
   else
   {
      printf("\nCould not write SD file <%s>\n",SD_fn);
   }
}

void Event_Loop(void)
{
 while(1)
 {
      SDL_Event e;
      while (SDL_PollEvent(&e) > 0)
      {
         switch (e.type)
         {
            case SDL_EVENT_QUIT: SDL_DestroyWindow(EmuWin);
                           if (SD) Save_SD();
                           SDL_Quit();
                           Print_Status(lf);
                           return;
            case SDL_EVENT_KEY_DOWN: Key_Event(e); break;
         }
      }
      Step();
   }
}

char *Find_Symbol(int a)
{
   int i;
   for (i=0 ; i < Symbols ; ++i)
   {
      if (a == SymVal[i]) return Symbol[i];
   }
   return NULL;
}

int Find_Address(char *s)
{
   int i;
   for (i=0 ; i < Symbols ; ++i)
   {
      if (!strcmp(s,Symbol[i])) return SymVal[i];
   }
   printf("*** [%s] not found\n",s);
   return 0;
}

#define IGMAX 15

char *Ignore[IGMAX] =
{
   "DUART_Init",
   "Do_SETLED",
   "VDP_SetReg",
   "VDP_VRAM_Target",
   "VDP_Create_Cursor",
   "VDP_WaitStatus",
   "OPL2_Reset",
   "OPL2_Write",
   "OPL2_Wait_Beep",
   "PS2_Keyb_Send",
   "PS2_Init",
   "BIOS_Print_Error",
   "SD_Card_Init",
   "TFM_Prolog",
   "TFM_Epilog"
};


char *Intercept[INMAX] =
{
   "VDP_Bank",            //  0
   "Zero_Bank",           //  1
   "Select_Bank",         //  2
   "VDP_ClearVRAM",       //  3
   "VDP_SyncChar",        //  4
   "Far_LDA",             //  5
   "Far_LDD",             //  6
   "Far_STA",             //  7
   "Far_STD",             //  8
   "VDP_ScrollUp_VRAM",   //  9
   "VDP_CursorOn",        // 10
   "VDP_CursorOff",       // 11
   "VDP_ScrollDown_VRAM", // 12
   "SD_Read_Block",       // 13
   "RTC_Read",            // 14
   "SD_Write_Block"       // 15
};


void Build_Intercept(void)
{
   int i;
   WORD a;

   for (i=0 ; i < IGMAX ; ++i)
   {
      a = Find_Address(Ignore[i]);
      ROM[a] = 0x39; // RTS
      if (lf) fprintf(lf,"Ignore    %4.4x %s\n",a,Ignore[i]);
   }
   for (i=0 ; i < INMAX ; ++i)
   {
      a = Find_Address(Intercept[i]);
      ROM[a] = 0xcf; // illegal
      Trap[i] = a;
      if (lf) fprintf(lf,"Intercept %4.4x %s\n",a,Intercept[i]);
   }
   TextColor = &RAM[0][Find_Address("BIOS_TextColor")];
   BGColor   = &RAM[0][Find_Address("BIOS_BGColor")];
   InsertMode= &RAM[0][Find_Address("BIOS_InsertMode")];
   CursorX   = &RAM[0][Find_Address("BIOS_CursorX")];
   CursorY   = &RAM[0][Find_Address("BIOS_CursorY")];
   CursorV   = &RAM[0][Find_Address("BIOS_CursorVis")];
   LBA_Ptr   = &RAM[0][Find_Address("BIOS_LBA")];
   B_Jump    = &RAM[0][Find_Address("BIOS_Jump")];
   SD_cb_sec = &RAM[0][Find_Address("SD_cb_sec")];
   KEYBUF    =         Find_Address("KEYBUF");
   keyrdp    =         Find_Address("keyrdp");
   keywrp    =         Find_Address("keywrp");
   lastkey   = &RAM[0][Find_Address("lastkey")];
   Put_Word(KEYBUF,keyrdp);
   Put_Word(KEYBUF,keywrp);
}

void reset(void)
{
   int a,i,m;
   char *s;
   char *v;

   Acc.Q = 0;

   Reg.X  = 0;
   Reg.Y  = 0;
   Reg.U  = 0;
   Reg.S  = 0;

   CC = 0;
   DP = 0;
   MD = 0;

   Bank = 0;
   Rom  = 1;

   if (lf)
   {
      fprintf(lf,"CPU reset\n---------\n");
      for (i=0,a=0xfff0 ; i<8 ; ++i,a+=2)
      {
         m = (ROM[a]<<8)|ROM[a+1];
         v = Find_Symbol(a);
         s = Find_Symbol(m);
         fprintf(lf,"%4.4x %-12s : %4.4x %s\n",a,v,m,s);
      }
   }

   PC = (ROM[0xfffe]<<8) | ROM[0xffff];
   Font_Start = &RAM[7][Find_Address("CHRGEN")];
}

void Read_Symbols(void)
{
   int i;
   FILE *rf;
   char Line[SLEN];

   rf = fopen(SYM_fn,"r");
   if (rf)
   {
      fgets(Line,sizeof(Line),rf);
      Symbols = atoi(Line);
      if (lf) fprintf(lf,"%d Symbols\n",Symbols);
      for (i=0 ; i < Symbols ; ++i)
      {
         fgets(Line,sizeof(Line),rf);
         Line[strlen(Line)-1] = 0;
         sscanf(Line+2,"%4x",SymVal+i);
         strncpy(Symbol[i],Line+7,LENSYM-1);
//       printf("%4d %4.4x <%s>\n",i,SymVal[i],Symbol[i]);
      }
      fclose(rf);
   }
   else
   {
      printf("\nCould not open SYM file <%s>\n",SYM_fn);
      exit(1);
   }
}

void Init_Run(void)
{
   int i;

   for (i=0 ; i < 256 ; ++i)
   {
      Opc[i] = Todo_Op;
      O10[i] = Todo_Op;
      O11[i] = Todo_Op;
      PoB[i] = Todo_PB;
   }
   for (i=0 ; i < 128 ; ++i)
   {
      PoB[i] = PoB_Offset;
   }

   Opc[0x00] = Opc_00_NEG_D;
   Opc[0x01] = Opc_01_OIM_D;
   Opc[0x02] = Opc_02_AIM_D;
   Opc[0x03] = Opc_03_COM_D;
   Opc[0x04] = Opc_04_LSR_D;
   Opc[0x05] = Opc_05_EIM_D;
   Opc[0x06] = Opc_06_ROR_D;
   Opc[0x07] = Opc_07_ASR_D;
   Opc[0x08] = Opc_08_LSL_D;
   Opc[0x09] = Opc_09_ROL_D;
   Opc[0x0a] = Opc_0A_DEC_D;
   Opc[0x0b] = Opc_0B_TIM_D;
   Opc[0x0c] = Opc_0C_INC_D;
   Opc[0x0d] = Opc_0D_TST_D;
   Opc[0x0e] = Opc_0E_JMP_D;
   Opc[0x0f] = Opc_0F_CLR_D;

   Opc[0x12] = Opc_12_NOP;
   Opc[0x13] = Todo_Op;      // SYNC
   Opc[0x14] = Opc_14_SEXW;
   Opc[0x15] = Opc_15_MON;
   Opc[0x16] = Opc_16_LBRA;
   Opc[0x17] = Opc_17_LBSR;
   Opc[0x19] = Opc_19_DAA;
   Opc[0x1a] = Opc_1A_ORCC;
   Opc[0x1c] = Opc_1C_ANDCC;
   Opc[0x1d] = Opc_1D_SEX;
   Opc[0x1e] = Opc_1E_EXG;
   Opc[0x1f] = Opc_1F_TFR;

   Opc[0x20] = Opc_20_BRA;
   Opc[0x21] = Opc_21_BRN;
   Opc[0x22] = Opc_22_BHI;
   Opc[0x23] = Opc_23_BLS;
   Opc[0x24] = Opc_24_BCC;
   Opc[0x25] = Opc_25_BCS;
   Opc[0x26] = Opc_26_BNE;
   Opc[0x27] = Opc_27_BEQ;
   Opc[0x28] = Opc_28_BVC;
   Opc[0x29] = Opc_29_BVS;
   Opc[0x2a] = Opc_2A_BPL;
   Opc[0x2b] = Opc_2B_BMI;
   Opc[0x2c] = Opc_2C_BGE;
   Opc[0x2d] = Opc_2D_BLT;
   Opc[0x2e] = Opc_2E_BGT;
   Opc[0x2f] = Opc_2F_BLE;

   Opc[0x30] = Opc_30_LEAX;
   Opc[0x31] = Opc_31_LEAY;
   Opc[0x32] = Opc_32_LEAS;
   Opc[0x33] = Opc_33_LEAU;
   Opc[0x34] = Opc_34_PSHS;
   Opc[0x35] = Opc_35_PULS;
   Opc[0x36] = Opc_36_PSHU;
   Opc[0x37] = Opc_37_PULU;
   Opc[0x39] = Opc_39_RTS;
   Opc[0x3a] = Opc_3A_ABX;
   Opc[0x3b] = Opc_3B_RTI;
   Opc[0x3c] = Todo_Op;      // CWAI
   Opc[0x3d] = Opc_3D_MUL;
   Opc[0x3f] = Opc_3F_SWI;

   Opc[0x40] = Opc_40_NEGA;
   Opc[0x43] = Opc_43_COMA;
   Opc[0x44] = Opc_44_LSRA;
   Opc[0x46] = Opc_46_RORA;
   Opc[0x47] = Opc_47_ASRA;
   Opc[0x48] = Opc_48_ASLA;
   Opc[0x49] = Opc_49_ROLA;
   Opc[0x4a] = Opc_4A_DECA;
   Opc[0x4c] = Opc_4C_INCA;
   Opc[0x4d] = Opc_4D_TSTA;
   Opc[0x4f] = Opc_4F_CLRA;

   Opc[0x50] = Opc_50_NEGB;
   Opc[0x53] = Opc_53_COMB;
   Opc[0x54] = Opc_54_LSRB;
   Opc[0x56] = Opc_56_RORB;
   Opc[0x57] = Opc_57_ASRB;
   Opc[0x58] = Opc_58_ASLB;
   Opc[0x59] = Opc_59_ROLB;
   Opc[0x5a] = Opc_5A_DECB;
   Opc[0x5c] = Opc_5C_INCB;
   Opc[0x5d] = Opc_5D_TSTB;
   Opc[0x5f] = Opc_5F_CLRB;

   Opc[0x60] = Opc_60_NEG_P;
   Opc[0x61] = Opc_61_OIM_P;
   Opc[0x62] = Opc_62_AIM_P;
   Opc[0x63] = Opc_63_COM_P;
   Opc[0x64] = Opc_64_LSR_P;
   Opc[0x65] = Opc_65_EIM_P;
   Opc[0x66] = Opc_66_ROR_P;
   Opc[0x67] = Opc_67_ASR_P;
   Opc[0x68] = Opc_68_LSL_P;
   Opc[0x69] = Opc_69_ROL_P;
   Opc[0x6a] = Opc_6A_DEC_P;
   Opc[0x6b] = Opc_6B_TIM_P;
   Opc[0x6c] = Opc_6C_INC_P;
   Opc[0x6d] = Opc_6D_TST_P;
   Opc[0x6e] = Opc_6E_JMP_P;
   Opc[0x6f] = Opc_6F_CLR_P;

   Opc[0x70] = Opc_70_NEG_E;
   Opc[0x71] = Opc_71_OIM_E;
   Opc[0x72] = Opc_72_AIM_E;
   Opc[0x73] = Opc_73_COM_E;
   Opc[0x74] = Opc_74_LSR_E;
   Opc[0x75] = Opc_75_EIM_E;
   Opc[0x76] = Opc_76_ROR_E;
   Opc[0x77] = Opc_77_ASR_E;
   Opc[0x78] = Opc_78_LSL_E;
   Opc[0x79] = Opc_79_ROL_E;
   Opc[0x7a] = Opc_7A_DEC_E;
   Opc[0x7b] = Opc_7B_TIM_E;
   Opc[0x7c] = Opc_7C_INC_E;
   Opc[0x7d] = Opc_7D_TST_E;
   Opc[0x7e] = Opc_7E_JMP_E;
   Opc[0x7f] = Opc_7F_CLR_E;

   Opc[0x80] = Opc_80_SUBA_I;
   Opc[0x81] = Opc_81_CMPA_I;
   Opc[0x82] = Opc_82_SBCA_I;
   Opc[0x83] = Opc_83_SUBD_I;
   Opc[0x84] = Opc_84_ANDA_I;
   Opc[0x85] = Opc_85_BITA_I;
   Opc[0x86] = Opc_86_LDA_I;
   Opc[0x88] = Opc_88_EORA_I;
   Opc[0x89] = Opc_89_ADCA_I;
   Opc[0x8a] = Opc_8A_ORA_I;
   Opc[0x8b] = Opc_8B_ADDA_I;
   Opc[0x8c] = Opc_8C_CMPX_I;
   Opc[0x8d] = Opc_8D_BSR;
   Opc[0x8e] = Opc_8E_LDX_I;

   Opc[0x90] = Opc_90_SUBA_D;
   Opc[0x91] = Opc_91_CMPA_D;
   Opc[0x92] = Opc_92_SBCA_D;
   Opc[0x93] = Opc_93_SUBD_D;
   Opc[0x94] = Opc_94_ANDA_D;
   Opc[0x95] = Opc_95_BITA_D;
   Opc[0x96] = Opc_96_LDA_D;
   Opc[0x97] = Opc_97_STA_D;
   Opc[0x98] = Opc_98_EORA_D;
   Opc[0x99] = Opc_99_ADCA_D;
   Opc[0x9a] = Opc_9A_ORA_D;
   Opc[0x9b] = Opc_9B_ADDA_D;
   Opc[0x9c] = Opc_9C_CMPX_D;
   Opc[0x9d] = Opc_9D_JSR_D;
   Opc[0x9e] = Opc_9E_LDX_D;
   Opc[0x9f] = Opc_9F_STX_D;

   Opc[0xa0] = Opc_A0_SUBA_P;
   Opc[0xa1] = Opc_A1_CMPA_P;
   Opc[0xa2] = Opc_A2_SBCA_P;
   Opc[0xa3] = Opc_A3_SUBD_P;
   Opc[0xa4] = Opc_A4_ANDA_P;
   Opc[0xa5] = Opc_A5_BITA_P;
   Opc[0xa6] = Opc_A6_LDA_P;
   Opc[0xa7] = Opc_A7_STA_P;
   Opc[0xa8] = Opc_A8_EORA_P;
   Opc[0xa9] = Opc_A9_ADCA_P;
   Opc[0xaa] = Opc_AA_ORA_P;
   Opc[0xab] = Opc_AB_ADDA_P;
   Opc[0xac] = Opc_AC_CMPX_P;
   Opc[0xad] = Opc_AD_JSR_P;
   Opc[0xae] = Opc_AE_LDX_P;
   Opc[0xaf] = Opc_AF_STX_P;

   Opc[0xb0] = Opc_B0_SUBA_E;
   Opc[0xb1] = Opc_B1_CMPA_E;
   Opc[0xb2] = Opc_B2_SBCA_E;
   Opc[0xb3] = Opc_B3_SUBD_E;
   Opc[0xb4] = Opc_B4_ANDA_E;
   Opc[0xb5] = Opc_B5_BITA_E;
   Opc[0xb6] = Opc_B6_LDA_E;
   Opc[0xb7] = Opc_B7_STA_E;
   Opc[0xb8] = Opc_B8_EORA_E;
   Opc[0xb9] = Opc_B9_ADCA_E;
   Opc[0xba] = Opc_BA_ORA_E;
   Opc[0xbb] = Opc_BB_ADDA_E;
   Opc[0xbc] = Opc_BC_CMPX_E;
   Opc[0xbd] = Opc_BD_JSR_E;
   Opc[0xbe] = Opc_BE_LDX_E;
   Opc[0xbf] = Opc_BF_STX_E;

   Opc[0xc0] = Opc_C0_SUBB_I;
   Opc[0xc1] = Opc_C1_CMPB_I;
   Opc[0xc2] = Opc_C2_SBCB_I;
   Opc[0xc3] = Opc_C3_ADDD_I;
   Opc[0xc4] = Opc_C4_ANDB_I;
   Opc[0xc5] = Opc_C5_BITB_I;
   Opc[0xc6] = Opc_C6_LDB_I;
   Opc[0xc8] = Opc_C8_EORB_I;
   Opc[0xc9] = Opc_C9_ADCB_I;
   Opc[0xca] = Opc_CA_ORB_I;
   Opc[0xcb] = Opc_CB_ADDB_I;
   Opc[0xcc] = Opc_CC_LDD_I;
   Opc[0xcd] = Opc_CD_LDQ_I;
   Opc[0xce] = Opc_CE_LDU_I;
   Opc[0xcf] = Opc_CF_Trap;

   Opc[0xd0] = Opc_D0_SUBB_D;
   Opc[0xd1] = Opc_D1_CMPB_D;
   Opc[0xd2] = Opc_D2_SBCB_D;
   Opc[0xd3] = Opc_D3_ADDD_D;
   Opc[0xd4] = Opc_D4_ANDB_D;
   Opc[0xd5] = Opc_D5_BITB_D;
   Opc[0xd6] = Opc_D6_LDB_D;
   Opc[0xd7] = Opc_D7_STB_D;
   Opc[0xd8] = Opc_D8_EORB_D;
   Opc[0xd9] = Opc_D9_ADCB_D;
   Opc[0xda] = Opc_DA_ORB_D;
   Opc[0xdb] = Opc_DB_ADDB_D;
   Opc[0xdc] = Opc_DC_LDD_D;
   Opc[0xdd] = Opc_DD_STD_D;
   Opc[0xde] = Opc_DE_LDU_D;
   Opc[0xdf] = Opc_DF_STU_D;

   Opc[0xe0] = Opc_E0_SUBB_P;
   Opc[0xe1] = Opc_E1_CMPB_P;
   Opc[0xe2] = Opc_E2_SBCB_P;
   Opc[0xe3] = Opc_E3_ADDD_P;
   Opc[0xe4] = Opc_E4_ANDB_P;
   Opc[0xe5] = Opc_E5_BITB_P;
   Opc[0xe6] = Opc_E6_LDB_P;
   Opc[0xe7] = Opc_E7_STB_P;
   Opc[0xe8] = Opc_E8_EORB_P;
   Opc[0xe9] = Opc_E9_ADCB_P;
   Opc[0xea] = Opc_EA_ORB_P;
   Opc[0xeb] = Opc_EB_ADDB_P;
   Opc[0xec] = Opc_EC_LDD_P;
   Opc[0xed] = Opc_ED_STD_P;
   Opc[0xee] = Opc_EE_LDU_P;
   Opc[0xef] = Opc_EF_STU_P;

   Opc[0xf0] = Opc_F0_SUBB_E;
   Opc[0xf1] = Opc_F1_CMPB_E;
   Opc[0xf2] = Opc_F2_SBCB_E;
   Opc[0xf3] = Opc_F3_ADDD_E;
   Opc[0xf4] = Opc_F4_ANDB_E;
   Opc[0xf5] = Opc_F5_BITB_E;
   Opc[0xf6] = Opc_F6_LDB_E;
   Opc[0xf7] = Opc_F7_STB_E;
   Opc[0xf8] = Opc_F8_EORB_E;
   Opc[0xf9] = Opc_F9_ADCB_E;
   Opc[0xfa] = Opc_FA_ORB_E;
   Opc[0xfb] = Opc_FB_ADDB_E;
   Opc[0xfc] = Opc_FC_LDD_E;
   Opc[0xfd] = Opc_FD_STD_E;
   Opc[0xfe] = Opc_FE_LDU_E;
   Opc[0xff] = Opc_FF_STU_E;

   O10[0x21] = O10_21_LBRN;
   O10[0x22] = O10_22_LBHI;
   O10[0x23] = O10_23_LBLS;
   O10[0x24] = O10_24_LBCC;
   O10[0x25] = O10_25_LBCS;
   O10[0x26] = O10_26_LBNE;
   O10[0x27] = O10_27_LBEQ;
   O10[0x28] = O10_28_LBVC;
   O10[0x29] = O10_29_LBVS;
   O10[0x2a] = O10_2A_LBPL;
   O10[0x2b] = O10_2B_LBMI;
   O10[0x2c] = O10_2C_LBGE;
   O10[0x2d] = O10_2D_LBLT;
   O10[0x2e] = O10_2E_LBGT;
   O10[0x2f] = O10_2F_LBLE;

   O10[0x30] = O10_30_ADDR;
   O10[0x31] = O10_31_ADCR;
   O10[0x32] = O10_32_SUBR;
   O10[0x33] = O10_33_SBCR;
   O10[0x34] = O10_34_ANDR;
   O10[0x35] = O10_35_ORR;
   O10[0x36] = O10_36_EORR;
   O10[0x37] = O10_37_CMPR;
   O10[0x38] = O10_38_PSHSW;
   O10[0x39] = O10_39_PULSW;
   O10[0x3a] = O10_3A_PSHUW;
   O10[0x3b] = O10_3B_PULUW;
   O10[0x3f] = O10_3F_SWI2;

   O10[0x40] = O10_40_NEGD;
   O10[0x43] = O10_43_COMD;
   O10[0x44] = O10_44_LSRD;
   O10[0x46] = O10_46_RORD;
   O10[0x47] = O10_47_ASRD;
   O10[0x48] = O10_48_ASLD;
   O10[0x49] = O10_49_ROLD;
   O10[0x4a] = O10_4A_DECD;
   O10[0x4c] = O10_4C_INCD;
   O10[0x4d] = O10_4D_TSTD;
   O10[0x4f] = O10_4F_CLRD;

   O10[0x53] = O10_53_COMW;
   O10[0x54] = O10_54_LSRW;
   O10[0x56] = O10_56_RORW;
   O10[0x59] = O10_59_ROLW;
   O10[0x5a] = O10_5A_DECW;
   O10[0x5c] = O10_5C_INCW;
   O10[0x5d] = O10_5D_TSTW;
   O10[0x5f] = O10_5F_CLRW;

   O10[0x80] = O10_80_SUBW_I;
   O10[0x81] = O10_81_CMPW_I;
   O10[0x82] = O10_82_SBCD_I;
   O10[0x83] = O10_83_CMPD_I;
   O10[0x84] = O10_84_ANDD_I;
   O10[0x85] = O10_85_BITD_I;
   O10[0x86] = O10_86_LDW_I;
   O10[0x88] = O10_88_EORD_I;
   O10[0x89] = O10_89_ADCD_I;
   O10[0x8a] = O10_8A_ORD_I;
   O10[0x8b] = O10_8B_ADDW_I;
   O10[0x8c] = O10_8C_CMPY_I;
   O10[0x8e] = O10_8E_LDY_I;

   O10[0x90] = O10_90_SUBW_D;
   O10[0x91] = O10_91_CMPW_D;
   O10[0x92] = O10_92_SBCD_D;
   O10[0x93] = O10_93_CMPD_D;
   O10[0x94] = O10_94_ANDD_D;
   O10[0x95] = O10_95_BITD_D;
   O10[0x96] = O10_96_LDW_D;
   O10[0x97] = O10_97_STW_D;
   O10[0x98] = O10_98_EORD_D;
   O10[0x99] = O10_99_ADCD_D;
   O10[0x9a] = O10_9A_ORD_D;
   O10[0x9b] = O10_9B_ADDW_D;
   O10[0x9c] = O10_9C_CMPY_D;
   O10[0x9e] = O10_9E_LDY_D;
   O10[0x9f] = O10_9F_STY_D;

   O10[0xa0] = O10_A0_SUBW_P;
   O10[0xa1] = O10_A1_CMPW_P;
   O10[0xa2] = O10_A2_SBCD_P;
   O10[0xa3] = O10_A3_CMPD_P;
   O10[0xa4] = O10_A4_ANDD_P;
   O10[0xa5] = O10_A5_BITD_P;
   O10[0xa6] = O10_A6_LDW_P;
   O10[0xa7] = O10_A7_STW_P;
   O10[0xa8] = O10_A8_EORD_P;
   O10[0xa9] = O10_A9_ADCD_P;
   O10[0xaa] = O10_AA_ORD_P;
   O10[0xab] = O10_AB_ADDW_P;
   O10[0xac] = O10_AC_CMPY_P;
   O10[0xae] = O10_AE_LDY_P;
   O10[0xaf] = O10_AF_STY_P;

   O10[0xb0] = O10_B0_SUBW_E;
   O10[0xb1] = O10_B1_CMPW_E;
   O10[0xb2] = O10_B2_SBCD_E;
   O10[0xb3] = O10_B3_CMPD_E;
   O10[0xb4] = O10_B4_ANDD_E;
   O10[0xb5] = O10_B5_BITD_E;
   O10[0xb6] = O10_B6_LDW_E;
   O10[0xb7] = O10_B7_STW_E;
   O10[0xb8] = O10_B8_EORD_E;
   O10[0xb9] = O10_B9_ADCD_E;
   O10[0xba] = O10_BA_ORD_E;
   O10[0xbb] = O10_BB_ADDW_E;
   O10[0xbc] = O10_BC_CMPY_E;
   O10[0xbe] = O10_BE_LDY_E;
   O10[0xbf] = O10_BF_STY_E;

   O10[0xce] = O10_CE_LDS_I;

   O10[0xdc] = O10_DC_LDQ_D;
   O10[0xdd] = O10_DD_STQ_D;
   O10[0xde] = O10_DE_LDS_D;
   O10[0xdf] = O10_DF_STS_D;

   O10[0xec] = O10_EC_LDQ_P;
   O10[0xed] = O10_ED_STQ_P;
   O10[0xee] = O10_EE_LDS_P;
   O10[0xef] = O10_EF_STS_P;

   O10[0xfc] = O10_FC_LDQ_E;
   O10[0xfd] = O10_FD_STQ_E;
   O10[0xfe] = O10_FE_LDS_E;
   O10[0xff] = O10_FF_STS_E;

   O11[0x30] = O11_3X_BITOP; // BAND
   O11[0x31] = O11_3X_BITOP; // BIAND
   O11[0x32] = O11_3X_BITOP; // BOR
   O11[0x33] = O11_3X_BITOP; // BIOR
   O11[0x34] = O11_3X_BITOP; // BEOR
   O11[0x35] = O11_3X_BITOP; // BIEOR
   O11[0x36] = O11_3X_BITOP; // LDBT
   O11[0x37] = O11_3X_BITOP; // STBT
   O11[0x38] = O11_3X_TFM;   // TFM RS++,RD++
   O11[0x39] = O11_3X_TFM;   // TFM RS--,RD--
   O11[0x3a] = O11_3X_TFM;   // TFM RS++,RD
   O11[0x3b] = O11_3X_TFM;   // TFM RS  ,RD++
   O11[0x3c] = O11_3C_BITMD;
   O11[0x3d] = O11_3D_LDMD;
   O11[0x3f] = O11_3F_SWI3;

   O11[0x43] = O11_43_COME;
   O11[0x4a] = O11_4A_DECE;
   O11[0x4c] = O11_4C_INCE;
   O11[0x4d] = O11_4D_TSTE;
   O11[0x4f] = O11_4F_CLRE;

   O11[0x53] = O11_53_COMF;
   O11[0x5a] = O11_5A_DECF;
   O11[0x5c] = O11_5C_INCF;
   O11[0x5d] = O11_5D_TSTF;
   O11[0x5f] = O11_5F_CLRF;

   O11[0x80] = O11_80_SUBE_I;
   O11[0x81] = O11_81_CMPE_I;
   O11[0x83] = O11_83_CMPU_I;
   O11[0x86] = O11_86_LDE_I;
   O11[0x8b] = O11_8B_ADDE_I;
   O11[0x8d] = O11_XD_DIVD;
   O11[0x8e] = O11_XE_DIVQ;
   O11[0x8f] = O11_XF_MULD;

   O11[0x90] = O11_90_SUBE_D;
   O11[0x91] = O11_91_CMPE_D;
   O11[0x93] = O11_93_CMPU_D;
   O11[0x96] = O11_96_LDE_D;
   O11[0x97] = O11_97_STE_D;
   O11[0x9b] = O11_9B_ADDE_D;
   O11[0x9d] = O11_XD_DIVD;
   O11[0x9e] = O11_XE_DIVQ;
   O11[0x9f] = O11_XF_MULD;

   O11[0xa0] = O11_A0_SUBE_P;
   O11[0xa1] = O11_A1_CMPE_P;
   O11[0xa3] = O11_A3_CMPU_P;
   O11[0xa6] = O11_A6_LDE_P;
   O11[0xa7] = O11_A7_STE_P;
   O11[0xab] = O11_AB_ADDE_P;
   O11[0xad] = O11_XD_DIVD;
   O11[0xae] = O11_XE_DIVQ;
   O11[0xaf] = O11_XF_MULD;

   O11[0xb0] = O11_B0_SUBE_E;
   O11[0xb1] = O11_B1_CMPE_E;
   O11[0xb3] = O11_B3_CMPU_E;
   O11[0xb6] = O11_B6_LDE_E;
   O11[0xb7] = O11_B7_STE_E;
   O11[0xbb] = O11_BB_ADDE_E;
   O11[0xbd] = O11_XD_DIVD;
   O11[0xbe] = O11_XE_DIVQ;
   O11[0xbf] = O11_XF_MULD;

   O11[0xc0] = O11_C0_SUBF_I;
   O11[0xc1] = O11_C1_CMPF_I;
   O11[0xc6] = O11_C6_LDF_I;
   O11[0xcb] = O11_CB_ADDF_I;

   O11[0xd0] = O11_D0_SUBF_D;
   O11[0xd1] = O11_D1_CMPF_D;
   O11[0xd6] = O11_D6_LDF_D;
   O11[0xd7] = O11_D7_STF_D;
   O11[0xdb] = O11_DB_ADDF_D;

   O11[0xe0] = O11_E0_SUBF_P;
   O11[0xe1] = O11_E1_CMPF_P;
   O11[0xe6] = O11_E6_LDF_P;
   O11[0xe7] = O11_E7_STF_P;
   O11[0xeb] = O11_EB_ADDF_P;

   O11[0xf0] = O11_F0_SUBF_E;
   O11[0xf1] = O11_F1_CMPF_E;
   O11[0xf6] = O11_F6_LDF_E;
   O11[0xf7] = O11_F7_STF_E;
   O11[0xfb] = O11_FB_ADDF_E;

// *********
// Post byte
// *********

   PoB[0x80] = PoB_PINC; // X+
   PoB[0xa0] = PoB_PINC; // Y+
   PoB[0xc0] = PoB_PINC; // U+
   PoB[0xe0] = PoB_PINC; // S+

   PoB[0x81] = PoB_PINC; // X++
   PoB[0xa1] = PoB_PINC; // Y++
   PoB[0xc1] = PoB_PINC; // U++
   PoB[0xe1] = PoB_PINC; // S++

   PoB[0x82] = PoB_PDEC; //  -X
   PoB[0xa2] = PoB_PDEC; //  -Y
   PoB[0xc2] = PoB_PDEC; //  -U
   PoB[0xe2] = PoB_PDEC; //  -S

   PoB[0x83] = PoB_PDEC; // --X
   PoB[0xa3] = PoB_PDEC; // --Y
   PoB[0xc3] = PoB_PDEC; // --U
   PoB[0xe3] = PoB_PDEC; // --S

   PoB[0x84] = PoB_REG;  // X
   PoB[0xa4] = PoB_REG;  // Y
   PoB[0xc4] = PoB_REG;  // U
   PoB[0xe4] = PoB_REG;  // S

   PoB[0x85] = PoB_BR;   // B,X
   PoB[0xa5] = PoB_BR;   // B,Y
   PoB[0xc5] = PoB_BR;   // B,U
   PoB[0xe5] = PoB_BR;   // B,S

   PoB[0x86] = PoB_AR;   // A,X
   PoB[0xa6] = PoB_AR;   // A,Y
   PoB[0xc6] = PoB_AR;   // A,U
   PoB[0xe6] = PoB_AR;   // A,S

   PoB[0x87] = PoB_ER;   // E,X
   PoB[0xa7] = PoB_ER;   // E,Y
   PoB[0xc7] = PoB_ER;   // E,U
   PoB[0xe7] = PoB_ER;   // E,S

   PoB[0x88] = PoB_OF8;  // nn,X
   PoB[0xa8] = PoB_OF8;  // nn,Y
   PoB[0xc8] = PoB_OF8;  // nn,U
   PoB[0xe8] = PoB_OF8;  // nn,S

   PoB[0x89] = PoB_O16;  // nnnn,X
   PoB[0xa9] = PoB_O16;  // nnnn,Y
   PoB[0xc9] = PoB_O16;  // nnnn,U
   PoB[0xe9] = PoB_O16;  // nnnn,S

   PoB[0x8a] = PoB_FR;   // F,X
   PoB[0xaa] = PoB_FR;   // F,Y
   PoB[0xca] = PoB_FR;   // F,U
   PoB[0xea] = PoB_FR;   // F,S

   PoB[0x8b] = PoB_DR;   // D,X
   PoB[0xab] = PoB_DR;   // D,Y
   PoB[0xcb] = PoB_DR;   // D,U
   PoB[0xeb] = PoB_DR;   // D,S

   PoB[0x8c] = PoB_PC8;  // nn,PC
   PoB[0xac] = PoB_PC8;  // nn,PC
   PoB[0xcc] = PoB_PC8;  // nn,PC
   PoB[0xec] = PoB_PC8;  // nn,PC

   PoB[0x8d] = PoB_P16;  // nnnn,PC
   PoB[0xad] = PoB_P16;  // nnnn,PC
   PoB[0xcd] = PoB_P16;  // nnnn,PC
   PoB[0xed] = PoB_P16;  // nnnn,PC

   PoB[0x8e] = PoB_WR;   // W,X
   PoB[0xae] = PoB_WR;   // W,Y
   PoB[0xce] = PoB_WR;   // W,U
   PoB[0xee] = PoB_WR;   // W,S

   PoB[0x8f] = PoB_W;    // W
   PoB[0xaf] = PoB_WOW;  // nnnn,W
   PoB[0xcf] = PoB_WPL;  // W++
   PoB[0xef] = PoB_MIW;  // --W

   PoB[0x90] = PoB_W;    // [W]
   PoB[0xb0] = PoB_WOW;  // [nnnn,W]
   PoB[0xd0] = PoB_WPL;  // [W++]
   PoB[0xf0] = PoB_MIW;  // [--W]

   PoB[0x91] = PoB_PINC; // [X++]
   PoB[0xb1] = PoB_PINC; // [Y++]
   PoB[0xd1] = PoB_PINC; // [U++]
   PoB[0xf1] = PoB_PINC; // [S++]

   PoB[0x93] = PoB_PDEC; // [--X]
   PoB[0xb3] = PoB_PDEC; // [--Y]
   PoB[0xd3] = PoB_PDEC; // [--U]
   PoB[0xf3] = PoB_PDEC; // [--S]

   PoB[0x94] = PoB_REG;  // [X]
   PoB[0xb4] = PoB_REG;  // [Y]
   PoB[0xd4] = PoB_REG;  // [U]
   PoB[0xf4] = PoB_REG;  // [S]

   PoB[0x95] = PoB_BR;   // [B,X]
   PoB[0xb5] = PoB_BR;   // [B,Y]
   PoB[0xd5] = PoB_BR;   // [B,U]
   PoB[0xf5] = PoB_BR;   // [B,S]

   PoB[0x96] = PoB_AR;   // [A,X]
   PoB[0xb6] = PoB_AR;   // [A,Y]
   PoB[0xd6] = PoB_AR;   // [A,U]
   PoB[0xf6] = PoB_AR;   // [A,S]

   PoB[0x97] = PoB_ER;   // [E,X]
   PoB[0xb7] = PoB_ER;   // [E,Y]
   PoB[0xd7] = PoB_ER;   // [E,U]
   PoB[0xf7] = PoB_ER;   // [E,S]

   PoB[0x98] = PoB_OF8;  // [nn,X]
   PoB[0xb8] = PoB_OF8;  // [nn,Y]
   PoB[0xd8] = PoB_OF8;  // [nn,U]
   PoB[0xf8] = PoB_OF8;  // [nn,S]

   PoB[0x99] = PoB_O16;  // [nnnn,X]
   PoB[0xb9] = PoB_O16;  // [nnnn,Y]
   PoB[0xd9] = PoB_O16;  // [nnnn,U]
   PoB[0xf9] = PoB_O16;  // [nnnn,S]

   PoB[0x9a] = PoB_FR;   // [F,X]
   PoB[0xba] = PoB_FR;   // [F,Y]
   PoB[0xda] = PoB_FR;   // [F,U]
   PoB[0xfa] = PoB_FR;   // [F,S]

   PoB[0x9b] = PoB_DR;   // [D,X]
   PoB[0xbb] = PoB_DR;   // [D,Y]
   PoB[0xdb] = PoB_DR;   // [D,U]
   PoB[0xfb] = PoB_DR;   // [D,S]

   PoB[0x9c] = PoB_PC8;  // [nn,PC]
   PoB[0xbc] = PoB_PC8;  // [nn,PC]
   PoB[0xdc] = PoB_PC8;  // [nn,PC]
   PoB[0xfc] = PoB_PC8;  // [nn,PC]

   PoB[0x9d] = PoB_P16;  // [nnnn,PC]
   PoB[0xbd] = PoB_P16;  // [nnnn,PC]
   PoB[0xdd] = PoB_P16;  // [nnnn,PC]
   PoB[0xfd] = PoB_P16;  // [nnnn,PC]

   PoB[0x9e] = PoB_WR;   // [W,X]
   PoB[0xbe] = PoB_WR;   // [W,Y]
   PoB[0xde] = PoB_WR;   // [W,U]
   PoB[0xfe] = PoB_WR;   // [W,S]

   PoB[0x9f] = PoB_IND;  // [nnnn]
}

void usage(void)
{
   printf("\nspecht -s SD_file\n");
}

void Read_ROM(void)
{
   size_t n;
   FILE *rf;
   rf = fopen(ROM_fn,"rb");
   if (rf)
   {
      n = fread(ROM+0x8000,1,0x8000,rf);
      fclose(rf);
   }
   else
   {
      printf("\nCould not open ROM file <%s>\n",ROM_fn);
      exit(1);
   }
   ROM[0xfeed] = 0;
}

void Read_SD_Image(void)
{
   FILE *sd;
   sd = fopen(SD_fn,"rb");
   if (sd)
   {
      fseek(sd,0,SEEK_END);
      SD_Size = ftell(sd);
      SD = malloc(SD_Size);
      rewind(sd);
      fread(SD,1,SD_Size,sd);
      fclose(sd);
      fprintf(lf,"SD Size = %ld blocks\n",SD_Size/512);
   }
   else
   {
      printf("\nCould not open SD file <%s>\n",SD_fn);
   }

}

int main(int argc, char *argv[])
{
   int ic;
   if (Log) lf=fopen("specht.log","w");
   for (ic=1 ; ic < argc ; ++ic)
   {
      if (!strcmp(argv[ic],"-s")) SD_fn = argv[++ic];
   }
   Read_ROM();
   Read_Symbols();
   Read_SD_Image();
   Build_Intercept();
   *SD_cb_sec = 255;
   Init_SDL();
   reset();
   Init_Run();
   Event_Loop();
   return 0;
}

