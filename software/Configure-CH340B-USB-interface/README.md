USB Interface konfigurieren (CH340B)
====================================

Das im Sparrow verwendete USB-Seriell-Interface CH340B stammt von der Firma Nanjing Qinheng Microelectronics Co., Ltd. / WinChipHead (WCH). 

Es enthält einen kleinen nichtflüchtigen Speicher, der mittels eines durch den Hersteller gelieferten Windows-Programms konfiguriert werden kann. Damit kann der Sparrow fortan direkt als solcher erkannt werden, wenn er an einem PC oder Mac angeschlossen wird. Diese Konfiguration ist nur einmalig nach dem Bestücken der Hauptplatine des Sparrows erforderlich. 

Die benötigte Software ist im git repository abgelegt, kann aber auch direkt vom Hersteller unter folgender URL bezogen werden: [CH34xSerCfg.ZIP - NanjingQinhengMicroelectronics](https://www.wch-ic.com/downloads/CH34xSerCfg_ZIP.html)

Der Ablauf der Konfiguration ist wie folgt:

- Sparrow einschalten
- Sparrow über USB-Kabel direkt ohne zwischengeschalteten USB-Hub mit Windows-PC verbinden 
- CH34xSerCfg.exe starten, "Möchten Sie zulassen, dass durch diese App Änderungen an Ihrem Gerät vorgenommen werden?" mit "Ja" beantworten
- Im USB-Baum links im Programm muss nun ein "USB SERIAL CH340" mit zugewiesener COM-Port-Nummer auftauchen
- Auf den Butten "Erase EEPROM" klicken ![Alternativer Text](101-conf-ch340b.png)
- Oben links auf "LoadCfg" klicken und die Datei "sparrow.cfg" durch Doppelklick laden
- Rechts muss jetzt bei "Product string" das Häkchen gesetzt sein und "sparrow09" eingetragen sein, dann die Seriennummer darunter anpassen. Die Gesamtzahl der Ziffern sollte unverändert bleiben. ![Alternativ Text](102-conf-ch340b.png)
- Auf den Button "SetConfig" klicken, wenig später muss unten links die Status-Meldung "Operation status: Write configuration successfully" erscheinen, damit wurde der Vorgang erfolgreich abgeschlossen und das Programm kann beendet werden. ![Alternativ Text](103-conf-ch340b.png)
