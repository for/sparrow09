# sparrow09
6309 Retro Homebrew Computer

![Photo of 6309 Single Board Computer](doc/pictures/6309_sbc_pcb3.jpg)

SW2 ON --> VDP/PS2 Terminal, otherwise:
SW1 ON --> USB console, OFF --> RS-232 console
